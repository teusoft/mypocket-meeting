//
//  QuickTipsView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/27/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "QuickTipsView.h"
#import "Profile.h"
#import "AFNetworking.h"
#import "Lib.h"
#import "ServiceLib.h"
#import "JSONKit.h"
#import "LCoreData.h"

#import "Mixpanel.h"

@interface QuickTipsView ()

@end

@implementation QuickTipsView {
    Profile *profileObject;
    NSMutableArray *arrTipsId;
    NSMutableArray *arrLikeStatus;
    int pageCurrent;
}

@synthesize moodType;

- (void)viewDidLoad
{
    [super viewDidLoad];

    profileObject = [LCoreData getProfile];
    
    scrollContent.hidden = YES;
    pageControl.hidden = YES;
    
    
    pageCurrent = 1;
    
    pageControlBeingUsed = NO;

    scrollContent.contentSize = CGSizeMake(921, 223);
}

- (void)loadTipForMoodId:(int)moodId
{
    moodType = moodId;
    [self serviceGetQuickTips];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnClose:(id)sender {
    
    [UIView animateWithDuration:.25f animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = scrollContent.frame.size.width;
		int page = floor((scrollContent.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		pageControl.currentPage = page;
        pageCurrent = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (IBAction)changePage:(id)sender {
    // Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = scrollContent.frame.size.width * pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = scrollContent.frame.size;
	[scrollContent scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsed = YES;
}

// check email for registration
- (void)serviceGetQuickTips
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *serviceURL = [NSString stringWithFormat:@"%@%@", SERVICE_URL, GET_QUICK_TIPS_SERVICE];
    
    NSDictionary *params = @{@"token": profileObject.userToken, @"moodId":[NSNumber numberWithInt:moodType]};
    
    NSString* serviceTitle = @"GET QUICK TIPS";
    [Lib logRequestWithName:serviceTitle url:serviceURL params:params];
    
    
    NSOperation *op=[manager POST:serviceURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                NSArray* feeds = (NSArray*)[results objectForKey:MPM_SERVICE_RESPONSE_DATA];
                if(feeds){
                    scrollContent.hidden = NO;
                    pageControl.hidden = NO;
                    NSMutableArray *arrTips = [[NSMutableArray alloc]init];
                    
                    arrTipsId = [[NSMutableArray alloc]init];
                    arrLikeStatus = [[NSMutableArray alloc]init];
                    
                    NSArray *arrTextView = @[txtTips1, txtTips2, txtTips3];
                    NSArray* arrLikeButton = @[btnLike1, btnLike2, btnLike3];
                    for (int i = 0; i < feeds.count; i++) {
                        @try{
                        NSDictionary *currentObject  = (NSDictionary*) [feeds objectAtIndex:i];
                        if (currentObject) {
                            
                            [arrTips addObject:[currentObject objectForKey:@"Content"]];
                            UIButton* btn = [arrLikeButton objectAtIndex:i];
                            [btn setTitle:[[currentObject objectForKey:@"IsLiked"] boolValue] ? [Lib localizedWithKey:@"MPM_KEY_LIKED"]:[Lib localizedWithKey:@"MPM_KEY_LIKE"] forState:UIControlStateNormal];
                            NSNumber* tagId = (NSNumber*)[currentObject objectForKey:@"Id"];
                            btn.tag = [tagId integerValue];
                            UITextView *textView = [arrTextView objectAtIndex:i];
                            textView.font = MPM_FONT_PRO_LIGHT(15);
                            NSString* sr =[currentObject objectForKey:@"Content"];
                            textView.text = sr != nil ? sr : @"";
                        }
                        }
                        @catch(NSException *ex)
                        {
                        
                        }
                    }
                    
                    [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                }else{
                    [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                }
            }else{
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        [Lib showAlertMessage:[error localizedDescription]];
        
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}

// check email for registration
- (void)serviceLikeQuickTipAtButton:(UIButton*)likeBtn
{
    
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *serviceURL = [NSString stringWithFormat:@"%@%@", SERVICE_URL, POST_LIKE_TIPS_SERVICE];
    
    NSNumber* tagNumber =[NSNumber numberWithInteger:likeBtn.tag];
    if (tagNumber==nil) {
        NSLog(@"tagNumber nill");
        return;
    }
    NSDictionary *params = @{@"token":profileObject.userToken,@"tipId":tagNumber};
    
    NSString* serviceTitle = @"LIKE QUICK TIP";
    [Lib logRequestWithName:serviceTitle url:serviceURL params:params];
    
    
    NSOperation *op=[manager POST:serviceURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;
        if (results) {
            
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                // Track User Like a tip
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                
                [mixpanel track:MPM_MIX_PANEL_KEY_LIKE_TIP properties:@{@"Tip ID": tagNumber}];
                
                NSString* title = [[results objectForKey:MPM_SERVICE_RESPONSE_DATA] boolValue] ? [Lib localizedWithKey:@"MPM_KEY_LIKE"]:[Lib localizedWithKey:@"MPM_KEY_LIKED"]  ;
                [likeBtn setTitle:title forState:UIControlStateNormal];
                
                //[Lib showAlertMessage:@"Thanks for your like"];
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        [Lib showAlertMessage:[error localizedDescription]];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}

- (IBAction)btnLike:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn) {
        [self serviceLikeQuickTipAtButton:btn];
    }
}


@end
