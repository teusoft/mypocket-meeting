//
//  MyProfileView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCommentPopup.h"
#import "QuickTipsView.h"
#import "EventDetailViewController.h"
#import <EventKit/EventKit.h>
#import "GoogleOAuth.h"
#import "Mixpanel.h"
#import "IntroViewController.h"
#import "QuickTipsView.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Profile.h"
#import "MoodTimeline.h"
#import "AddCommentPopup.h"
#import "MoodCell.h"
#import "Lib.h"
#import "ObjEvent.h"
#import "LCoreData.h"
#import "ObjShare.h"
#import "MoodTimeline.h"
#import "UIView+ColorOfPoint.h"
#import "AFNetworking.h"
#import "JSONKit.h"
#import "SlideTimerView.h"
#import "SmileyView.h"

#import "MPMCalendarObject.h"



@interface MyProfileView : UIViewController <UITableViewDataSource, UITableViewDelegate, AddCommentPopupDelegate, QuickTipsViewDelegate, UITextFieldDelegate, EventDetaiDelegate, GoogleOAuthDelegate, UIScrollViewDelegate, SmileyViewDelegate, SlideTimerViewDelegate, UIAlertViewDelegate> {
    
    
    __weak IBOutlet UITableView *tblTimeline;
    
    __weak IBOutlet UILabel *lbNetworkStatus;
    __weak IBOutlet UIButton *btnSkip;
    
    __weak IBOutlet UIActivityIndicatorView *loadingIndicator;
    
    
    QuickTipsView *tipView;
    IntroViewController *introView;
    
    
    //---------------------
    __weak IBOutlet UIScrollView *mScrollView;
    
    __weak IBOutlet UIImageView *viewTimeLine;
    
    __weak IBOutlet UIImageView *cloudCalendarIcon;
    
    NSDateFormatter* dateFormat;
    
    
    //---------------------
    
    BOOL isOnMeeting;
    
    
    int currentPage;
    BOOL isLoadingMoreData;
    
    BOOL isSyncDone;
    int countDone;
    int totalLocalMood;
    
    int selectedCalendar;
    BOOL isLoadingCalendar;
    
    NSMutableArray* listSubViews;
}

@property (nonatomic, retain) NSOperationQueue *operationQueue;

@property (nonatomic, strong) GoogleOAuth *googleOAuth;

@property (nonatomic, strong) NSMutableArray *arrGoogleCalendars;

@property (nonatomic, strong) EKEventStore *eventStore;

// Default calendar associated with the above event store
@property (nonatomic, strong) EKCalendar *defaultCalendar;

// Array of all events happening within the next 24 hours
@property (nonatomic, strong) NSMutableArray *eventsList;

@property (strong, nonatomic) IBOutlet UILabel *lblMyMeetings;

@property (strong, nonatomic) IBOutlet UILabel *lblTextTimer;
@property (strong, nonatomic) SlideTimerView *slideTimerView;

@property (strong, nonatomic) AddCommentPopup           *addCommentPopup;
@property (strong, nonatomic) EventDetailViewController *eventDetailPopup;


@property (strong, nonatomic) IBOutlet UITabBarItem *tabBarItemProfile;
//--------------

- (IBAction)btnRefreshEventClicked:(id)sender;

- (IBAction)btnShowSmiley:(id)sender;

//--------------

@end
