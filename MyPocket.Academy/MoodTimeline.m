//
//  MoodTimeline.m
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/17/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "MoodTimeline.h"


@implementation MoodTimeline

@dynamic dateTime;
@dynamic isPosted;
@dynamic moodAfter;
@dynamic moodBefore;
@dynamic moodId;
@dynamic note;
@dynamic title;
@dynamic moodAfterColor;
@dynamic moodBeforeColor;
@dynamic showDateTime;
@end
