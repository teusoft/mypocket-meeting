//
//  HelpView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "HelpView.h"
#import "MyProfileView.h"
//#import "PopUpHelpView.h"

@interface HelpView ()

@end

int touchCount;

@implementation HelpView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"HelpView: %@", self);
    _lblVersion.text = [Lib appBuildVersionNumber];
    _txtContent.text = [Lib localizedWithKey:@"MPM_KEY_DESCRIPTION_1"];
    _lblHelpMood.text = [Lib localizedWithKey:@"MPM_KEY_SMILEY_INSTRUCTION_1"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self setFont];
    
    UIImage *img = [UIImage imageNamed:@"icon_help"];
    UIImage *imgSelected = [UIImage imageNamed:@"icon_helpSelected"];
    
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imgSelected = [imgSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    self.tabBarItem.image = img;
    self.tabBarItem.selectedImage = imgSelected;
}

- (void)viewDidAppear:(BOOL)animated
{
    touchCount = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setFont {
    _txtContent.font = MPM_FONT_PRO_LIGHT(14);
}

- (IBAction)btnMoodHelp:(id)sender {
    touchCount = touchCount+1;
    if (touchCount == 0) {
        _txtContent.text = [Lib localizedWithKey:@"MPM_KEY_DESCRIPTION_1"];
        _lblHelpMood.text = [Lib localizedWithKey:@"MPM_KEY_SMILEY_INSTRUCTION_1"];
    } else if (touchCount == 1) {
        _txtContent.text = [Lib localizedWithKey:@"MPM_KEY_DESCRIPTION_2"];
        _lblHelpMood.text = [Lib localizedWithKey:@"MPM_KEY_SMILEY_INSTRUCTION_2"];
    } else if (touchCount == 2) {
        _txtContent.text = [Lib localizedWithKey:@"MPM_KEY_DESCRIPTION_3"];
        _lblHelpMood.text = [Lib localizedWithKey:@"MPM_KEY_SMILEY_INSTRUCTION_3"];
    } else if (touchCount == 3) {
        
        _txtContent.text = [Lib localizedWithKey:@"MPM_KEY_DESCRIPTION_4"];
        _lblHelpMood.text = [Lib localizedWithKey:@"MPM_KEY_SMILEY_INSTRUCTION_4"];
        

//    PopUpHelpView *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"popUpHelpView"];
//        PopUpHelpView *controller = [[PopUpHelpView alloc] initWithNibName:@"PopUpHelpView" bundle:nil];
//        controller.view.frame = CGRectMake(0, 0, controller.view.frame.size.width, controller.view.frame.size.height);
//        [self.view addSubview:controller.view];
        
        /////////////////////
        for(UITabBarItem *item in self.tabBarController.tabBar.items) {
            item.enabled = NO;
        }
        
        _popUpHelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"popUpHelpView"];
        _popUpHelpView.delegate = self;
        [_popUpHelpView showPopUp:self.view animated:YES];

        
//        MyProfileView *myProfileView = [self.storyboard instantiateViewControllerWithIdentifier:@"myProfileView"];
//        
//        int y = 1;
//        myProfileView.isFromHelpView = &(y);
//        
//        [self.tabBarController setSelectedIndex:0];
        ///////////////////
        
        
//    PopUpHelpView *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"popUpHelpView"];
//    [self presentViewController:controller animated:NO completion:nil];
        
        touchCount = 0;
        
    }
}

#pragma mark-PopUpHelpViewDelegate

- (void)popUpHelpViewDidSelect{
    
    for(UITabBarItem *item in self.tabBarController.tabBar.items) {
        item.enabled = YES;
    }
    
    [self.tabBarController setSelectedIndex:0];
}

@end
