//
//  MPMCalendarObject.m
//  MyPocket Meeting
//
//  Created by Nguyen Sy Tan on 12/11/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "MPMCalendarObject.h"

@implementation MPMCalendarObject

+ (MPMCalendarObject*)initWithDictionary:(NSDictionary *)dic
{
    MPMCalendarObject* mObj = [MPMCalendarObject new];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    mObj.MPM_Title = [dic objectForKey:@"summary"];
    mObj.MPM_StartDate    = [dateFormat dateFromString:[[dic objectForKey:@"start"] objectForKey:@"dateTime"]];
    mObj.MPM_ID = [dic objectForKey:@"id"];
    mObj.MPM_TimeZone = dateFormat.timeZone;
    
    return mObj;
}

@end
