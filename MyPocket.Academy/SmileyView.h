//
//  SmileyView.h
//  MyPocket Meeting
//
//  Created by Nguyen Sy Tan on 12/11/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lib.h"
#import "LCoreData.h"
#import "MoodTimeline.h"

@class SmileyView;

@protocol SmileyViewDelegate


@optional

- (void)smileyViewTableViewCellDidTimeOut:(SmileyView*)cell;
- (void)smileyViewTableViewCellDidCancel:(SmileyView*)cell;
- (void)smileyViewTableViewCellDidTapShowDetail:(SmileyView*)cell withTitle:(NSString*)title atDate:(NSDate*)mDate;

- (void)smileyViewTableViewCellDidTapTips:(SmileyView*)cell withId:(int)tipId;
- (void)smileyViewTableViewCellDidTapTimer:(SmileyView*)cell;
- (void)smileyViewTableViewCell:(SmileyView *)cell didStartWithMoodIndex:(int)moodId;

- (void)smileyViewTableViewCell:(SmileyView*)cell didFinishWithMoodTimeLine:(MoodTimeline*)moodEvent;
@end

@interface SmileyView : UIView<UITextFieldDelegate, UIAlertViewDelegate>
{
    int secondsCountDown;
    __weak IBOutlet UIScrollView *mScrollView;
    __weak IBOutlet UIButton* btnShowDetail;
    
    __weak IBOutlet UIButton* btnCancelNewEvent;
    __weak IBOutlet UIButton* btnTipsNewEvent;
    __weak IBOutlet UIImageView *viewMoodAfter;
    __weak IBOutlet UITextField *txtMeetingName;
    
    __weak IBOutlet UIView *viewSetTime;
    __weak IBOutlet UIView *viewFinish;
    
    
    __weak IBOutlet UISlider *sliderMoodAfter;
    
    __weak IBOutlet UILabel *lblMoodSelectionHeader;
    __weak IBOutlet UILabel *lblBefore;
    __weak IBOutlet UILabel *lblAfter;
    
    IBOutletCollection(UIButton) NSArray *smileyCollection;
    NSMutableArray* smileyFrame;
    BOOL isExpanded;
    
    NSTimer* mTimer;
    
    __weak  IBOutlet UILabel *lblTimer;
    
    int mBeforeMood;
    int mAfterMood;
    MoodTimeline* mMoodTimeLine;
}


@property (nonatomic, assign) id<SmileyViewDelegate> delegate;
@property (nonatomic, assign) BOOL shouldBeRemoved;
@property (nonatomic, assign) BOOL isRemoved;
@property (nonatomic, assign) NSDate* startDate;

- (void)setMeetingTitle:(NSString*)title startAt:(NSDate*)startAt;
- (BOOL)checkIsOutOfTime;
- (void)changeTimer:(int)secondsChange;

- (void)showCancelButton:(BOOL)neededShow;
- (NSString*)getTitle;
- (NSDate*)getDate;

- (IBAction)titleClicked:(id)sender;

+(id)initWithPoint:(CGPoint)point;



@end
