//
//  Lib.m
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/31/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "Lib.h"
#import <CommonCrypto/CommonHMAC.h>
#import "Reachability.h"
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>



@implementation UILabel(ExtendedMethods)
-(void)willDismissAfterSeconds:(int)seconds
{
    [self performSelector:@selector(clearText:) withObject:nil afterDelay:seconds];
}
- (void)clearText:(id)sender
{
    self.text = @"";
}
- (void)dissmissNow{
    self.text = @"";
}

@end


@implementation NSDate (ExtendedMethods)


- (int)compareToDate:(NSDate*)mDate
{
    if ([self compare:mDate] == NSOrderedDescending) {
        NSLog(@"%@ is later than %@", self, mDate);
        return 1;
    } else if ([self compare:mDate] == NSOrderedAscending) {
        NSLog(@"%@ is earlier than %@", self, mDate);
        return -1;
    } else {
        NSLog(@"%@, %@ are the same", self, mDate);
        return 0;
    }
}
@end


@implementation UIImage (Crop)

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)crop:(CGRect)rect {
    if (self.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * self.scale,
                          rect.origin.y * self.scale,
                          rect.size.width * self.scale,
                          rect.size.height * self.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}
@end

const NSString *emailRegEx =
@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
@"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
@"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
@"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
@"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
@"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
@"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
const NSString *numberRegEx = @"[0-9]+";


@implementation UIView(Extended)
- (UIImage *) imageByRenderingView {
	UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
@end

@implementation NSDictionary(Extended)
- (id)getObjectForKey:(id)aKey{
    if (![self objectForKey:aKey]||[[self objectForKey:aKey] isKindOfClass:[NSNull class]]) {
        return @"";
    }else{
        return [self objectForKey:aKey];
    }
}
@end

@implementation NSMutableDictionary(Extended)
- (void)setTheObject:(id)anObject forKey:(id <NSCopying>)aKey{
    if (!anObject||[anObject isKindOfClass:[NSNull class]]) {
        NSLog(@"do nothing with Dictionary");
    }else{
        [self setObject:anObject forKey:aKey];
    }
}
@end

@implementation UIColor (HexColors)

+(UIColor *)colorWithHexString:(NSString *)hexString {
    
    if ([hexString length] != 6) {
        return nil;
    }
    
    // Brutal and not-very elegant test for non hex-numeric characters
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-fA-F|0-9]" options:0 error:NULL];
    NSUInteger match = [regex numberOfMatchesInString:hexString options:NSMatchingReportCompletion range:NSMakeRange(0, [hexString length])];
    
    if (match != 0) {
        return nil;
    }
    
    NSRange rRange = NSMakeRange(0, 2);
    NSString *rComponent = [hexString substringWithRange:rRange];
    NSUInteger rVal = 0;
    NSScanner *rScanner = [NSScanner scannerWithString:rComponent];
    [rScanner scanHexInt:&rVal];
    float rRetVal = (float)rVal / 254;
    
    
    NSRange gRange = NSMakeRange(2, 2);
    NSString *gComponent = [hexString substringWithRange:gRange];
    NSUInteger gVal = 0;
    NSScanner *gScanner = [NSScanner scannerWithString:gComponent];
    [gScanner scanHexInt:&gVal];
    float gRetVal = (float)gVal / 254;
    
    NSRange bRange = NSMakeRange(4, 2);
    NSString *bComponent = [hexString substringWithRange:bRange];
    NSUInteger bVal = 0;
    NSScanner *bScanner = [NSScanner scannerWithString:bComponent];
    [bScanner scanHexInt:&bVal];
    float bRetVal = (float)bVal / 254;
    
    return [UIColor colorWithRed:rRetVal green:gRetVal blue:bRetVal alpha:1.0f];
    
}

+(NSString *)hexValuesFromUIColor:(UIColor *)color {
    
    if (!color) {
        return nil;
    }
    
    if (color == [UIColor whiteColor]) {
        // Special case, as white doesn't fall into the RGB color space
        return @"ffffff";
    }
    
    CGFloat red;
    CGFloat blue;
    CGFloat green;
    CGFloat alpha;
    
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    
    int redDec = (int)(red * 255);
    int greenDec = (int)(green * 255);
    int blueDec = (int)(blue * 255);
    
    NSString *returnString = [NSString stringWithFormat:@"%02x%02x%02x", (unsigned int)redDec, (unsigned int)greenDec, (unsigned int)blueDec];
    
    return returnString;
    
}

@end

@implementation NSString (Utilities)
- (BOOL) isEmail {
	NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
	return [regExPredicate evaluateWithObject:self];
}
- (NSString*)trim {
    return [self stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString*)stringWithWithCapitalizeFirstLetter{
    return [self stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[self substringToIndex:1] capitalizedString]];
}

- (BOOL)isNullOrEmpty
{
    return self == nil || [self isEqualToString:@""] ? YES : NO;
}


- (NSString*)append:(NSString*)str
{
    return [NSString stringWithFormat:@"%@%@", self, str];
}


- (NSString*)appendAtNewLine:(NSString*)str
{
    return [NSString stringWithFormat:@"%@\n%@", self, str];
}
@end



@implementation NSMutableArray (ExtendedMethods)
- (void)reverse {
    NSInteger i = 0;
    NSInteger j = [self count] - 1;
    while (i < j) {
        [self exchangeObjectAtIndex:i
                  withObjectAtIndex:j];
        i++;
        j--;
    }
}
- (void)shuffle
{
    NSUInteger count = [self count];
    for (uint i = 0; i < count; ++i)
    {
        // Select a random element between i and end of array to swap with.
        int nElements = count - i;
        int n = arc4random_uniform(nElements) + i;
        [self exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}


@end

static NSMutableDictionary* settings;
static NSString* _dbFilePath = nil; 
@implementation Lib

+ (BOOL)isIpad{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return NO;
    }
    return YES;
}

+(NSURL*)URLFromString:(NSString*)urlStr {
//    return [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    return [NSURL URLWithString:urlStr];
}

+ (NSString *) isNull:(id)object{
    if (!object||[object isKindOfClass:[NSNull class]]) {
        return @"";
    }else{
        return [NSString stringWithFormat:@"%@", object];
    }
}

+ (NSArray*)sortListData:(NSArray*)dataSource sortWithKey:(NSString*)myKey isAsc:(BOOL)isAsc
{
	NSArray * result = nil;
	
	if(dataSource.count >0){
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:myKey ascending:isAsc];
		NSArray * tmpListEpisodeSorted = [dataSource sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
		result = tmpListEpisodeSorted;
	}
	return result;
}
+(BOOL)object:(id)object classNamed:(NSString*)name {
    return [[[object class] description] isEqualToString:name];
}


//+(NSString*)imageBase64:(UIImage*)image {
//    NSString* retStr = nil;
//    NSData* data = UIImageJPEGRepresentation(image, 1.0);
//    if (data) {
//        retStr = [data base64EncodedString];
//    }
//    return retStr;
//}
//+(NSString*)randomID {
//    return [NSString stringWithFormat:@"%i%00i",[[NSDate date] timeIntervalSince1970],arc4random() % 999];
//}
//
//+ (NSString *)GetUUID{
//    CFUUIDRef theUUID = CFUUIDCreate(NULL);
//    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
//    CFRelease(theUUID);
//    return (NSString *)string;
//}

+(NSString*)docDirPath {
    return [NSHomeDirectory() stringByAppendingString:@"/Documents"];
}

+(void)showConfirmAlert:(NSString*)title withMessage:(NSString*)msg delegate:(UIViewController*)delegate{
    if (!title || [title isEqual: @""]) {
        title = MPM_ALERT_TITLE;
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:delegate
                                              cancelButtonTitle:@"YES"
                                              otherButtonTitles:@"NO",nil];
    [alertView show];
    alertView = nil;
}


+ (void)showAlertMessage:(NSString*)msg
{
    [self showAlert:MPM_ALERT_TITLE withMessage:NSLocalizedString(msg, msg)];
}

+(void)showAlert:(NSString*)title withMessage:(NSString*)msg {
    if (!title || [title isEqual: @""]) {
        title = MPM_ALERT_TITLE;
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg 
                                                       delegate:nil 
                                              cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_OK"]
                                              otherButtonTitles:nil];
    [alertView show];
    alertView = nil;
}


+(id)getValueOfKey:(NSString*)key {	
	if (!settings) {
		settings = [[NSUserDefaults standardUserDefaults] objectForKey:@"Mileage.settings"];
		if (!settings) {
			settings = [[NSMutableDictionary alloc] init];
		}
	}
	if ([settings objectForKey:key]) {
		return [settings objectForKey:key];
	}
	
	return nil;
}
+(void)setValue:(id)value ofKey:(NSString*)key {
	if (!settings) {
		settings = [[NSUserDefaults standardUserDefaults] objectForKey:@"Mileage.settings"];
		if (!settings) {
			settings = [[NSMutableDictionary alloc] init];
		}
	}
    if (value) {
        [settings setObject:value forKey:key];        
    } else {
        [settings removeObjectForKey:key];
    }
    
	[[NSUserDefaults standardUserDefaults] setObject:settings forKey:@"Mileage.settings"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)showLoadingViewOn2:(UIView *)aView withAlert:(NSString *)text{
	
	UIView *loadingView = [[UIView alloc] init];
	loadingView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin|
	UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
	loadingView.frame = ([Lib isIphone])?CGRectMake(0, 0, aView.frame.size.width, aView.frame.size.height):CGRectMake(0, 0, aView.frame.size.width, aView.frame.size.height-80);
	loadingView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
	loadingView.tag = 1011;
	UILabel *loadingLabel = [[UILabel alloc ] init];
	
	UIView* roundedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 80)];
	roundedView.center = CGPointMake(loadingView.frame.size.width/2, loadingView.frame.size.height/2);
	roundedView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
	roundedView.layer.borderColor = [UIColor clearColor].CGColor;
	roundedView.layer.borderWidth = 1.0;
	roundedView.layer.cornerRadius = 10.0;
	[loadingView addSubview:roundedView];
	roundedView.autoresizingMask = loadingLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
	
	loadingLabel.text = text;
	loadingLabel.frame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y + 50, 200, 30);
	//loadingLabel.adjustsFontSizeToFitWidth = YES;
	loadingLabel.textAlignment = NSTextAlignmentCenter;
	loadingLabel.font = [UIFont boldSystemFontOfSize:14];	
	loadingLabel.backgroundColor = [UIColor clearColor];
	loadingLabel.textColor = [UIColor whiteColor];
	[loadingView addSubview:loadingLabel];
	
	UIActivityIndicatorView *activityIndication = [[UIActivityIndicatorView alloc] init];
	activityIndication.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	activityIndication.frame = CGRectMake((loadingView.frame.size.width - 30)/2,
										  roundedView.frame.origin.y + 15,
										  30,
										  30);
	
	[activityIndication startAnimating];	
	[loadingView addSubview:activityIndication];
	activityIndication.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;

	//	[activityIndication release];
	[aView addSubview:loadingView];
}

+ (void)removeLoadingViewOn:(UIView *)superView{
	for (UIView *aView in superView.subviews) {
		if ((aView.tag == 1011)  && [aView isKindOfClass:[UIView class]]) {
			[aView removeFromSuperview];
		}
	}
}

+ (void)showIndicatorViewOn2:(UIView *)aView{
    aView.userInteractionEnabled = NO;
	UIActivityIndicatorView *activityIndication = [[UIActivityIndicatorView alloc] init];
	activityIndication.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
	int width = [Lib isIphone]?20:30;
	activityIndication.frame = CGRectMake((aView.frame.size.width - width)/2,
										  (aView.frame.size.height-width)/2,
										  width,
										  width);
	
	activityIndication.tag = 1012;
	[activityIndication startAnimating];	
	activityIndication.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
	
	
	[aView addSubview:activityIndication];
}

+ (void)removeIndcatorViewOn:(UIView *)superView {
	for (UIView *aView in superView.subviews) {
		if ((aView.tag == 1012)  && [aView isKindOfClass:[UIActivityIndicatorView class]]) {
			[aView removeFromSuperview];
		}
	}
    superView.userInteractionEnabled = YES;
}

+(NSDate*)getThisWeek {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents* components = [cal components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components weekday] - 1))]; 
    NSDate *thisWeek  = [cal dateFromComponents:components];
    
//    [components setDay:([components day] - 7)];
//    NSDate *lastWeek  = [cal dateFromComponents:components];
//    
//    [components setDay:([components day] - ([components day] -1))]; 
//    NSDate *thisMonth = [cal dateFromComponents:components];
//    
//    [components setMonth:([components month] - 1)]; 
//    NSDate *lastMonth = [cal dateFromComponents:components];
//    
//    NSLog(@"today=%@",today);
//    NSLog(@"yesterday=%@",yesterday);
//    NSLog(@"%@",thisWeek);  
    //    NSLog(@"lastWeek=%@",lastWeek);
//    NSLog(@"thisMonth=%@",thisMonth);
//    NSLog(@"lastMonth=%@",lastMonth);
    return thisWeek;
}

+(NSDate*)getThisMonth {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents* components = [cal components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components day] -1))]; 
    NSDate *thisMonth = [cal dateFromComponents:components];
        
    //    [components setMonth:([components month] - 1)]; 
    //    NSDate *lastMonth = [cal dateFromComponents:components];
    //    
    //    NSLog(@"today=%@",today);
    //    NSLog(@"yesterday=%@",yesterday);
    //    NSLog(@"lastWeek=%@",lastWeek);
//    NSLog(@"thisMonth=%@",thisMonth);
    //    NSLog(@"lastMonth=%@",lastMonth);
    return thisMonth;
}

+(NSDate*)getLastMonth {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents* components = [cal components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components day] -1))];
    [components setMonth:([components month] - 1)]; 
    NSDate *lastMonth = [cal dateFromComponents:components];
    //    
    //    NSLog(@"today=%@",today);
    //    NSLog(@"yesterday=%@",yesterday);
    //    NSLog(@"lastWeek=%@",lastWeek);
    //NSLog(@"thisMonth=%@",thisMonth);
//    NSLog(@"lastMonth=%@",lastMonth);
    return lastMonth;
}
+(BOOL)isIphone {
    //NSString *deviceType = [UIDevice currentDevice].model;
    //return [deviceType isEqualToString:@"iPhone"];
    return !(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}

+ (BOOL)isPhone5{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (IS_HEIGHT_GTE_568) {
            return YES;
        }
        return NO;
    }
    return NO;
}

+ (void)customWebview : (UIWebView *)aWebView {
	// tranparent WebView
	aWebView.opaque = NO;
	aWebView.backgroundColor = [UIColor clearColor];
	// web view
	id scroller = [aWebView.subviews objectAtIndex:0];
	
	for (UIView *subView in [scroller subviews])
		if ([[[subView class] description] isEqualToString:@"UIImageView"])
			subView.hidden = YES;
    
}

+(void)saveString:(NSString*)_str forKey:(NSString*)_key {
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setObject:_str forKey:_key];
	[prefs synchronize];
}

+(NSString*)getStringForKey:(NSString*)_key {
	return [[NSUserDefaults standardUserDefaults] stringForKey:_key];
	
}



//+ (NSString *) encodeWithHmacsha1:(NSString *)secret andContent:(NSString*)content{
//    //content =@"{\"params\":[\"password\",\"dsdsds\",\"email\",\"dsdsds@gamil.com\"],\"jsonrpc\":\"2.0\",\"id\":\"5\",\"method\":\"user.login\"}";
//    
//    NSMutableString *newContent = [[NSMutableString alloc] initWithFormat:@"/api/"];
//    [newContent appendString:@"\n"];
//    [newContent appendString:content];
//
//    
//	const char *cKey  = [secret cStringUsingEncoding:NSASCIIStringEncoding];
//	const char *cData = [newContent cStringUsingEncoding:NSASCIIStringEncoding];
//	
//	unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
//	
//	CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
//	//NSLog(@"--- %i",sizeof(cHMAC));
//	NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
//	
//	NSString* hMacStr = [HMAC base64EncodedString];
//
//    int padding = [hMacStr length] % 4;
//    if (padding) hMacStr = [hMacStr stringByAppendingFormat:@"%@",[@"====" substringToIndex:4-padding]];
//    
//    NSLog(@"L: %i %@",[hMacStr length], hMacStr);
//    return hMacStr;
//}

+ (void) setTitle:(NSString*)title forNavigationItem:(UINavigationItem*)navItem{
	CGSize size = [title sizeWithFont:[UIFont boldSystemFontOfSize:20.0]];
	
	CGRect frame = CGRectMake(0, 0, size.width, 40);
	UILabel *label = [[UILabel alloc] initWithFrame:frame];
	
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont boldSystemFontOfSize:20.0];
	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = NSTextAlignmentCenter;
	label.textColor = [UIColor blackColor];
	label.text = title;	
	
	navItem.titleView = label;
}

+ (void) setTitle:(NSString*)title forNavigationItem:(UINavigationItem*)navItem withFontSize:(CGFloat) fontSize{
	CGSize size = [title sizeWithFont:[UIFont boldSystemFontOfSize:fontSize]];
	
	CGRect frame = CGRectMake(0, 0, size.width, 40);
	UILabel *label = [[UILabel alloc] initWithFrame:frame];
	
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont boldSystemFontOfSize:fontSize];
	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = NSTextAlignmentCenter;
	label.textColor = [UIColor blackColor];
	label.text = title;	
	
	navItem.titleView = label;
}

//+(NSString*)getCountryNameISO3166:(NSString*)countryCode{
//    NSString *countryName;
//    switch (countryCode) {
//        case @"AR":
//            
//            break;
//            
//        default:
//            break;
//    }
//}

+(void)saveImage:(UIImage*)image withPath:(NSString *)imagePath{
    
    image = [UIImage imageWithImage:image scaledToSize:CGSizeMake(189, 189*image.size.height/image.size.width)];
    
    NSData *imageData = UIImageJPEGRepresentation(image,1.0); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    NSString *fullPath = [NSHomeDirectory() stringByAppendingPathComponent:imagePath]; //add our image to the path
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
} 

+ (BOOL) checkRetina{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        return YES;
    } else {
        // non-Retina display
        return NO;
    }
}

+(UIImage*)scaleAndRotateImage:(UIImage *)image
{
    // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    int kMaxResolution = width;
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    CGFloat scaleRatio = 1;
    
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;

    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    scaleRatio=1;
    NSLog(@"ori:%d",orient);
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -1, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //    [self setRotatedImage:imageCopy];
    return imageCopy;
}

+ (UIColor *)randomColor{
    CGFloat red = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat green = ( arc4random() % 256 / 256.0 );  //  0.5 to 1.0, away from white
    CGFloat blue = ( arc4random() % 256 / 256.0 );  //  0.5 to 1.0, away from black
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    return color;
}

+ (void)showAlertNoInternetConnection{
    [Lib showAlertMessage:@"No Internet Connection. You can't create or edit data"];
}

+ (void)showAlertOfflineMode{
    [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_OFFLINE_MODE"]];
}
+ (NSString *)getFahrenheitFromCelsius:(NSString*)temp{
    float celsius = [temp floatValue];
    float fahrenheit = (celsius*9/5) + 32;
    return [NSString stringWithFormat:@"%0.1f",fahrenheit];
}

+ (NSString *)getKelvinFromCelsius:(NSString*)temp{
    float celsius = [temp floatValue];
    float kelvin = celsius + 273.15;
    return [NSString stringWithFormat:@"%0.1f",kelvin];
}

+(BOOL)checkInternetConnection{
    return [self checkInternetConnectionWithMessage:@""];
}

+(BOOL)checkInternetConnectionWithMessage{
    return [self checkInternetConnectionWithMessage:[Lib localizedWithKey:@"MPM_KEY_NO_INTERNET_CONNECTION"]];
}

+(BOOL)checkInternetConnectionWithMessage:(NSString*)warningMessage{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus]==NotReachable) {
        if ([warningMessage length] > 0) {
            [self showAlert:MPM_ALERT_TITLE withMessage:warningMessage];
        }
        
        return NO;
    }
    return YES;
}

+ (NSString *)timeFromSeconds:(double)totalSeconds showSeconds:(BOOL)show
{
    int seconds = (int)totalSeconds % 60;
    int minutes = (int)(totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (show) {
        return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    }else{
        return [NSString stringWithFormat:@"%02d:%02d",hours, minutes];
    }
    
    
}

+ (BOOL)internetConnected{
    NetworkStatus status = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (status != NotReachable) {
        return TRUE;
    }
    return FALSE;
}

+ (NSString *)filterStr:(NSString *)str withCharacters:(NSString *)charSet{
//    NSString *inputStr = [str lowercaseString];
    NSString *inputStr = str;
    NSMutableString *strippedString = [NSMutableString
                                       stringWithCapacity:inputStr.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:inputStr];
    NSCharacterSet *filter = [NSCharacterSet
                              characterSetWithCharactersInString:charSet];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:filter intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    
    return strippedString;
}


+ (NSDate*)timestampToDate:(NSString*)timeStampString
{
    if ([timeStampString isEqualToString:@"<null>"]) {
        return nil;
    }
    NSTimeInterval _interval=[timeStampString doubleValue];
    return [NSDate dateWithTimeIntervalSince1970:_interval];
    
}


+ (void)logToFile:(NSString *)message
{
    //Get the file path
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSString* content = FORMAT(@"%@: %@\n",[dateFormatter stringFromDate:[NSDate date]],message);
    
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"MyPocketMeeting.log"];
    
    //create file if it doesn't exist
    if(![[NSFileManager defaultManager] fileExistsAtPath:fileName])
        [[NSFileManager defaultManager] createFileAtPath:fileName contents:nil attributes:nil];
    
    //append text to file (you'll probably want to add a newline every write)
    NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [file seekToEndOfFile];
    [file writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@",message);
    [file closeFile];
}

+(void)logDictionary:(NSDictionary*)dic serviceName:(NSString*)serviceName
{
    if (dic) {
        for (NSString* key in [dic allKeys]) {
            [self logToFile:FORMAT(@"-----| %@ |-----| Params: | %@ = %@",serviceName,key,[dic objectForKey:key])];
        }
    }
}


+(void)logArray:(NSArray*)dic serviceName:(NSString*)serviceName
{
    if (dic) {
        for (id key in dic) {
            [self logToFile:FORMAT(@"-----| %@ |-----| %@",serviceName,key)];
        }
    }
}

+ (void)logRequestWithName:(NSString*)serviceName url:(NSString*)url params:(NSDictionary*)params
{
    [self logToFile:FORMAT(@"-----| %@ |-----| Start   |-------|",serviceName)];
    [self logToFile:FORMAT(@"-----| %@ |-----| Url:    | %@",serviceName,url)];
    [self logDictionary:params serviceName:serviceName];
    [self logToFile:FORMAT(@"-----| %@ |-----| Finish  |-------|",serviceName)];
}

+ (void)logResponseWithName:(NSString*)serviceName response:(NSString*)msg
{
    
    [self logToFile:FORMAT(@"-----| %@ |-----| Response| %@|",serviceName,msg)];
}

//check valid email
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


+ (NSString *)localizedWithKey:(NSString *)key
{
    return NSLocalizedString(key, key);
}




+ (void)resetUserData
{
    [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_NOTIFICATION_LIST];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:MPM_CLOUD_CALENDAR_NONE] forKey:MPM_USER_CLOUD_CALENDAR_MODE];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey: MPM_USER_NEEDED_TO_UPDATE_INFO];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:MPM_USER_NEEDED_TO_SHOW_INSTRUCTION];
    
    [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_SMILEY_VIEW_LIST];
    
    [self setLastEventDate];
}

+ (void)cleanSmileyViews
{
    [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_SMILEY_VIEW_LIST];
}

+ (void)setBadgeNumber
{
    NSMutableArray* mDic = [NSMutableArray arrayWithArray: (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_SMILEY_VIEW_LIST]];
    if (mDic) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[mDic count]];
    }else{
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}

+ (void)addSmileyViewWithTitle:(NSString*)title startDate:(NSDate*)mDate tag:(NSNumber*)mTag
{
    NSMutableArray* mDic = [NSMutableArray arrayWithArray: (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_SMILEY_VIEW_LIST]];
    if (mDic == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_SMILEY_VIEW_LIST];
        return [self addSmileyViewWithTitle:title startDate:mDate tag:mTag];
    }
 
    NSDictionary* mView = [NSDictionary dictionaryWithObjectsAndKeys:title, @"_title", mDate,@"_date", mTag, @"_tag", nil];
    [mDic addObject:mView];
    [[NSUserDefaults standardUserDefaults] setObject:mDic forKey:MPM_SMILEY_VIEW_LIST];
}

+ (NSArray*)getSmileyViews
{
    return  [NSMutableArray arrayWithArray: (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_SMILEY_VIEW_LIST]];

}

+ (BOOL)checkExistNotification:(NSString*)mId
{
    NSMutableArray* mDic = [NSMutableArray arrayWithArray: (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_NOTIFICATION_LIST]];
    if (mDic == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_NOTIFICATION_LIST];
        return [self checkExistNotification:mId];
    }
//    [self logToFile:FORMAT( @"Notification List contains %@ = %d",mId,[mDic containsObject:mId])];
    return [mDic containsObject:mId];
}


+ (void)addNotification:(NSString*)mId
{
    NSMutableArray* mDic = [NSMutableArray arrayWithArray: (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_NOTIFICATION_LIST]];
    if (mDic == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_NOTIFICATION_LIST];
        return [self addNotification:mId];
    }
    if (![mDic containsObject:mId]) {
        
        [mDic addObject :mId];
        [self logToFile:FORMAT(@"--- ADD NOTIFICATION %@", mId)];
        [[NSUserDefaults standardUserDefaults] setObject:mDic forKey:MPM_NOTIFICATION_LIST];
        
    }
   
}

+ (void)removeNotification:(NSString*)mId
{
    NSMutableArray* mDic = [NSMutableArray arrayWithArray: (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_NOTIFICATION_LIST]];
    if (mDic == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSArray alloc] init] forKey:MPM_NOTIFICATION_LIST];
        return;
    }
    if ([mDic containsObject:mId]) {
        
        [mDic removeObject:mId];
        [self logToFile:FORMAT(@"--- REMOVE NOTIFICATION %@", mId)];
        [[NSUserDefaults standardUserDefaults] setObject:mDic forKey:MPM_NOTIFICATION_LIST];
        
    }
    
}


+ (void)setLastEventDate
{
    NSDate* googleDate = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_LAST_EVENT_TIME_GOOGLE];
    NSDate* icloudDate = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_LAST_EVENT_TIME_ICLOUD];
    NSDate* mNow = [NSDate date];
    if (googleDate == nil || [googleDate compareToDate:mNow] < 1) {
        googleDate = mNow;
        [[NSUserDefaults standardUserDefaults] setObject:googleDate forKey:MPM_USER_LAST_EVENT_TIME_GOOGLE];
    }
    
    if (icloudDate == nil || [icloudDate compareToDate:mNow] < 1) {
        icloudDate = mNow;
        [[NSUserDefaults standardUserDefaults] setObject:icloudDate forKey:MPM_USER_LAST_EVENT_TIME_ICLOUD];
    }
}

+ (NSDate*)getLastEventDate
{
    int selectedCalendar = [self getCloudCalendarMode];
    if (selectedCalendar == MPM_CLOUD_CALENDAR_NONE) {
        return nil;
    }
    NSDate* mLastDate = selectedCalendar == MPM_CLOUD_CALENDAR_GOOGLE ? (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_LAST_EVENT_TIME_GOOGLE] : (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_LAST_EVENT_TIME_ICLOUD];
    
    [self logToFile:FORMAT(@"=== LAST DATE %@ ===", mLastDate)];
    
    return mLastDate;
}

+ (void)setLastEventDate:(NSDate*)mDate
{
    NSDate* lastDate = [self getLastEventDate];
    if ([mDate compareToDate:lastDate] < 1) {
        return;
    }
    NSInteger selectedCalendar = [self getCloudCalendarMode];
    if (selectedCalendar == MPM_CLOUD_CALENDAR_NONE) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:mDate forKey:selectedCalendar == MPM_CLOUD_CALENDAR_ICLOUD ? MPM_USER_LAST_EVENT_TIME_ICLOUD : MPM_USER_LAST_EVENT_TIME_GOOGLE];
}

+ (BOOL)isValidAge:(NSString*)age
{
    int mAge  = [age intValue];
    if (mAge > 0 && mAge < 180) {
        return YES;
    }
    [self showAlertMessage:@"Your age is invalid"];
    return NO;
}



+ (int)getCloudCalendarMode
{
    NSNumber *mNum = (NSNumber*)[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_CLOUD_CALENDAR_MODE];
    if (mNum) {
        int i = [mNum intValue];
        return i;
    }
    return -1;
}

+ (NSString *) appVersionNumber
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (NSString *) appBuildNumber
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+ (NSString *) appBuildVersionNumber
{
    NSString * version = [self appVersionNumber];
    NSString * build = [self appBuildNumber];
    
    NSString * versionBuild = [NSString stringWithFormat: @"v%@", version];
    
    if (![version isEqualToString: build]) {
        versionBuild = [NSString stringWithFormat: @"%@(%@)", versionBuild, build];
    }
    
    return versionBuild;
}

@end
