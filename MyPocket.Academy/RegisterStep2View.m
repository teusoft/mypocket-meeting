//
//  RegisterStep2View.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/29/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "RegisterStep2View.h"
#import "BSKeyboardControls.h"

@interface RegisterStep2View ()

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation RegisterStep2View


@synthesize strEmail, strPassword;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *fields = @[txtFullName, txtCompany, txtOccupation];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    //date picker
//    [self initializeTextFieldInputView];
    scrollContent.contentSize = CGSizeMake(scrollContent.frame.size.width, 540+10);
    
    
    birthDayView.hidden = YES;
    birthDayView.alpha = 0.0;
    birthDaySubView.layer.cornerRadius = 4;
    birthDaySubView.layer.masksToBounds = YES;
    birthDaySubView.clipsToBounds = YES;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideStep2:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)btnFinish:(id)sender {
    
    if ([txtFullName.text isNullOrEmpty] || [txtCompany.text isNullOrEmpty]|| [btnBirthday.titleLabel.text isNullOrEmpty] || [txtCountry.text isNullOrEmpty]) {
        [Lib showAlertMessage:@"MPM_KEY_ERROR_ALL_MUST_NOT_BE_EMPTY"];
    }else{
        [self serviceCreateAccountWithEmail:strEmail pwd:strPassword];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    [self btnFinish:nil];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    txtError.text = @"";
    [self.keyboardControls setActiveField:textField];
    
    CGPoint tempPoint = textField.center;
    tempPoint.x = 0;
    
    [scrollContent setContentOffset:tempPoint animated:YES];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)keyboardWillHideStep2: (NSNotification*) notif{
    [scrollContent setContentOffset:CGPointZero animated:YES];
}
#pragma mark end keyboard

#pragma - Check register service

//RegisterMyPocket(string email, string pwd, string name, bool gender, string age, string country, string company, string occupation)
// check email for registration
- (void)serviceCreateAccountWithEmail:(NSString*)email pwd:(NSString*)pwd
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"email": email, @"pwd":pwd, @"name":txtFullName.text, @"gender":gender.selectedSegmentIndex == 0?@"true":@"false", @"birthday": btnBirthday.titleLabel.text, @"country": txtCountry.text, @"company": txtCompany.text, @"occupation":txtOccupation.text};
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, CREATE_ACCOUNT_SERVICE];
    
    NSString* serviceTitle = @"REGISTER BY EMAIL";
    
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    
    NSOperation *op = [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
//            NSString* code = [results objectForKey:MPM_SERVICE_RESPONSE_CODE];
//            NSString* message = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
            
            if (isSuccess) {
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_CHECK_MAIL_FOR_ACTIVATE"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [Lib logResponseWithName:serviceTitle response: [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                txtError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            txtError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response: [error localizedDescription]];
        txtError.text = [error localizedDescription];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}


- (IBAction)chooseCountry:(id)sender
{
    CountriesController *countriesViewController = (CountriesController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CountriesViewController"];
    countriesViewController.delegate = self;
    countriesViewController.curCountryCode = @"Argentina";
    [self.navigationController pushViewController:countriesViewController animated:YES];
}

- (void)selectCountry:(NSString *)countryCode
{
    txtCountry.text = countryCode;
}


#pragma mark BirthDay View

- (IBAction)btnBirthDayClicked:(id)sender
{
    mDatePicker.date = [NSDate date];
    mDatePicker.maximumDate = [NSDate date];
    [self showAnimateBirthDay];
}

- (void)showAnimateBirthDay
{
    birthDayView.transform = CGAffineTransformMakeScale(1.3, 1.3);
    birthDayView.alpha = 0;
    birthDayView.hidden = NO;
    [UIView animateWithDuration:.25 animations:^{
        birthDayView.alpha = 1;
        birthDayView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        if (finished) {
            birthDayView.userInteractionEnabled = YES;
        }
    }];
}

- (void)removeAnimateBirthDay
{
    [UIView animateWithDuration:.25 animations:^{
        birthDayView.transform = CGAffineTransformMakeScale(1.3, 1.3);
        birthDayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            birthDayView.hidden = YES;
        }
    }];
}



- (IBAction)dismissBirthDayView:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"M/d/yyyy"];
    [btnBirthday setTitle:[dateFormatter stringFromDate:mDatePicker.date] forState:UIControlStateNormal];
    
    [self removeAnimateBirthDay];
}


@end
