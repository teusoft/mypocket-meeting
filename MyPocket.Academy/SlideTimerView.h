//
//  SlideTimerView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/28/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SlideTimerViewDelegate

- (void)changeTimer:(int)secondsChange withTag:(int)tag;

@end

@interface SlideTimerView : UIViewController
@property int mtag;

@property (strong, nonatomic) IBOutlet UISlider *slideTimer;

@property (strong, nonatomic) IBOutlet UILabel *lblTimer;

@property (strong, nonatomic) IBOutlet UIView *sliderView;

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated withTag:(int)tag;

@property(nonatomic, strong)id<SlideTimerViewDelegate>delegate;

@end
