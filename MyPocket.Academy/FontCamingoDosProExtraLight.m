//
//  Font-CamingoDosPro-ExtraLight.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/3/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "FontCamingoDosProExtraLight.h"

@implementation FontCamingoDosProExtraLight

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = MPM_FONT_EXTRA_LIGHT(self.font.pointSize);
}

@end
