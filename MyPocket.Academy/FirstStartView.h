//
//  FirstStartView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/22/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstStartView : UIViewController{
    __weak IBOutlet UIImageView *backgroundImageSlide;
}

@end
