//
//  RegisterViewController.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/22/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "AFNetworking/AFNetworking.h"

@interface RegisterViewController : UIViewController <UITextFieldDelegate, BSKeyboardControlsDelegate>{
    
    __weak IBOutlet UIScrollView *scrollContent;
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblWelcome;
@property (strong, nonatomic) IBOutlet UILabel *lblLetStart;
@property (strong, nonatomic) IBOutlet UILabel *lblByContinuing;
@property (strong, nonatomic) IBOutlet UILabel *lblOr;
@property (strong, nonatomic) IBOutlet UILabel *lblDontWorry;


@property (strong, nonatomic) IBOutlet UIView *registerView;

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property (strong, nonatomic) IBOutlet UILabel *txtError;

@end
