//
//  LCoreData.m
//  ExpensesEntry
//
//  Created by Luthor on 10/8/13.
//  Copyright (c) 2013 Lex. All rights reserved.
//

#import "LCoreData.h"
#import "AppDelegate.h"
#import "Lib.h"

@implementation LCoreData

#pragma mark- Profile

+ (BOOL)addProfile:(NSDictionary*)itemData{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    Profile *postItem          = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:managedObjectContext];
    
    
    postItem.userToken        =  [itemData objectForKey:@"Token"];
    
    postItem.userId        = [itemData objectForKey:@"UserId"];
    
    postItem.name      = [Lib isNull:[itemData objectForKey:@"FullName"]];
    
    postItem.email       = [Lib isNull:[itemData objectForKey:@"Email"]];
    postItem.avatar       = [Lib isNull:[itemData objectForKey:@"Avatar"]];
    postItem.gender       = [NSNumber numberWithInt:[[itemData objectForKey:@"Gender"] intValue]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"M/d/yyyy"];
    NSDate *date = [dateFormat dateFromString:[itemData objectForKey:@"Birthday"] ];
    postItem.birthday       = date;
    postItem.country       = [Lib isNull:[itemData objectForKey:@"Country"]];
    postItem.company       = [Lib isNull:[itemData objectForKey:@"Company"]];
    postItem.occupation       = [Lib isNull:[itemData objectForKey:@"Occupation"]];
    postItem.lastLogin       = [Lib isNull:[itemData objectForKey:@"LastLogin"]];
    postItem.password       = [Lib isNull:[itemData objectForKey:@"Pwd"]];
    
    postItem.isFirstLogin       = [NSNumber numberWithBool:[[itemData objectForKey:@"IsFirstLogin"] boolValue]];
    postItem.isFacebook       = [NSNumber numberWithBool:[[itemData objectForKey:MPM_USER_IS_LOGGED_IN_BY_FACEBOOK] boolValue]];
    
    if(![managedObjectContext save:nil]) {
        NSLog(@"FAIL!");
        return NO;
    }
    
    return YES;
}

+ (NSInteger)splitTimeStamp:(NSString *)timeStamp {
    
    NSArray* arr1 = [timeStamp componentsSeparatedByString: @"("];
    NSString* str1 = [arr1 objectAtIndex: 1];
    NSArray* arr2 = [str1 componentsSeparatedByString: @")"];
    NSString* str2 = [arr2 objectAtIndex: 0];
    
    if (str2.length) {
        return [str2 integerValue];
    }
    
    return 0;
}

+ (BOOL)clearProfile{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Profile"];
    
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * itemEntry in fetchedObjects) {
        [managedObjectContext deleteObject:itemEntry];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    
    if (saveError!=nil) {
        return NO;
    }
    return YES;
}

+ (Profile *)getProfile{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Profile"];
    
    NSArray *arrQuery = [managedObjectContext executeFetchRequest:request error:nil];
    
    if (arrQuery.count >0) {
        return arrQuery[0];
    }
    
    return nil;
}

#pragma mark- MoodTimeline

+ (MoodTimeline*)creatNewMoodTimeline{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    MoodTimeline *postItem          = [NSEntityDescription insertNewObjectForEntityForName:@"MoodTimeline" inManagedObjectContext:managedObjectContext];
    postItem.isPosted = [NSNumber numberWithBool:NO];
    postItem.dateTime = [NSDate date];
    postItem.note = @"";
    
    return postItem;
}

+ (BOOL)addMoodTimeline:(NSDictionary*)itemData{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    MoodTimeline *postItem          = [NSEntityDescription insertNewObjectForEntityForName:@"MoodTimeline" inManagedObjectContext:managedObjectContext];
    
    postItem.moodId        = [itemData objectForKey:@"Id"];
    
    postItem.title        = [Lib isNull:[itemData objectForKey:@"Title"]];

    postItem.moodAfter        = [itemData objectForKey:@"MoodAfter"];
    postItem.moodBefore        = [itemData objectForKey:@"MoodBefore"];
    postItem.note        = [Lib isNull:[itemData objectForKey:@"Note"]];
    
    NSString *strDate = [Lib isNull:[itemData objectForKey:@"Datetime"]];
    strDate = [strDate componentsSeparatedByString:@"("][1];
    strDate = [strDate stringByReplacingOccurrencesOfString:@")/" withString:@""];
    strDate = [strDate substringToIndex:[strDate length]-3];

    postItem.dateTime        = [NSDate dateWithTimeIntervalSince1970:[strDate integerValue]];

    postItem.isPosted        = [NSNumber numberWithBool:YES];
    postItem.showDateTime = [itemData objectForKey:@"ShowDateTime"];

    if(![managedObjectContext save:nil]) {
        NSLog(@"FAIL!");
        return NO;
    }
    
    return YES;
}

+ (BOOL)clearOnlineMoodTimeline{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"MoodTimeline"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isPosted = %@", [NSNumber numberWithBool:YES]];

    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * itemEntry in fetchedObjects) {
        [managedObjectContext deleteObject:itemEntry];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    
    if (saveError!=nil) {
        return NO;
    }
    return YES;
}

+ (BOOL)clearAllMoodTimeline{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"MoodTimeline"];
    
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * itemEntry in fetchedObjects) {
        [managedObjectContext deleteObject:itemEntry];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    
    if (saveError!=nil) {
        return NO;
    }
    return YES;
}

+ (NSArray *)getAllMoodTimeline{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"MoodTimeline"];
    
    //Sort
    NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:@"dateTime" ascending:NO];
    
    [request setSortDescriptors:[NSArray arrayWithObjects:sort1, nil]];
    
    //Fetched result
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:request error:&error];
    
    return fetchedObjects;
}

+ (NSArray *)getLocalMoodTimeline{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"MoodTimeline"];
    request.predicate = [NSPredicate predicateWithFormat:@"isPosted = %@", [NSNumber numberWithBool:NO]];

    //Sort
    NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:@"moodId" ascending:NO];
    
    [request setSortDescriptors:[NSArray arrayWithObjects:sort1, nil]];
    
    //Fetched result
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:request error:&error];
    
    return fetchedObjects;
}

+ (BOOL)deleteMoodTimeline:(MoodTimeline*)moodTimelineItem{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    if (!moodTimelineItem) {
        return YES;
    }
    [managedObjectContext deleteObject:moodTimelineItem];
    
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    
    if (saveError!=nil) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Save

+ (BOOL)saveContext{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    if (saveError!=nil) {
        return NO;
    }
    return YES;
}

@end
