//
//  MoodCell.m
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 9/6/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "MoodCell.h"
#import "Lib.h"

#define kLabelHorizontalInsets      15.0f
#define kLabelVerticalInsets        10.0f

@implementation MoodCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDetailWithData:(MoodTimeline *) moodItem{
    
    NSArray *arrColor = @[@"cc0000", @"ff6600", @"e5ae35", @"849f34"];
    
    
    self.imgBefore.backgroundColor = [UIColor colorWithHexString:arrColor[[moodItem.moodBefore intValue]-1]];
    self.imgBefore.layer.cornerRadius = 24.f/2;
//    self.imgBefore.layer.borderWidth = 1.f;
//    self.imgBefore.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.imgAfter.backgroundColor = [UIColor colorWithHexString:arrColor[[moodItem.moodAfter intValue]-1]];
    self.imgAfter.layer.cornerRadius = 24.f/2;
//    self.imgAfter.layer.borderWidth = 1.f;
//    self.imgAfter.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"dd.MM.yyyy @ HH:mm"];
    
    self.lbTime.text = moodItem.showDateTime;
    
    self.lbComment.text = moodItem.note;
    self.lbTitle.text = moodItem.title;
    
    [self.lbComment sizeToFit];
    
    CGRect bgFrame = self.img_bg.frame;
    bgFrame.size.height = 54 + self.lbComment.frame.size.height;
    
    self.img_bg.frame = bgFrame;
    
    self.img_bg.image =[[UIImage imageNamed:@"bg_tableCell"] stretchableImageWithLeftCapWidth:0.f topCapHeight:10.f];
    
    if ([moodItem.isPosted boolValue]) {
        self.lbLocalWarning.hidden = YES;
    }else{
        self.lbLocalWarning.hidden = NO;
    }
}

//- (void)updateConstraints
//{
//    if (!self.didSetupConstraints) {
//        // Note: if the constraints you add below require a larger cell size than the current size (which is likely to be the default size {320, 44}), you'll get an exception.
//        // As a fix, you can temporarily increase the size of the cell's contentView so that this does not occur using code similar to the line below.
//        //      See here for further discussion: https://github.com/Alex311/TableCellWithAutoLayout/commit/bde387b27e33605eeac3465475d2f2ff9775f163#commitcomment-4633188
//        // self.contentView.bounds = CGRectMake(0.0f, 0.0f, 99999.0f, 99999.0f);
//        
//        [UIView autoSetPriority:UILayoutPriorityRequired forConstraints:^{
//            [self.titleLabel autoSetContentCompressionResistancePriorityForAxis:ALAxisVertical];
//        }];
//        [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:kLabelVerticalInsets];
//        [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kLabelHorizontalInsets];
//        [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelHorizontalInsets];
//        
//        [self.bodyLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.titleLabel withOffset:kLabelVerticalInsets];
//        
//        [UIView autoSetPriority:UILayoutPriorityRequired forConstraints:^{
//            [self.bodyLabel autoSetContentCompressionResistancePriorityForAxis:ALAxisVertical];
//        }];
//        [self.bodyLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kLabelHorizontalInsets];
//        [self.bodyLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelHorizontalInsets];
//        [self.bodyLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:kLabelVerticalInsets];
//        
//        self.didSetupConstraints = YES;
//    }
//    
//    [super updateConstraints];
//}

@end
