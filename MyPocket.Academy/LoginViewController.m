//
//  LoginViewController.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/22/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "LoginViewController.h"
#import "FirstStartView.h"
#import "MyProfileView.h"
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Profile.h"

#import "MenuTabBar.h"
#import "BSKeyboardControls.h"

#import "LCoreData.h"

#import "ServiceLib.h"
#import "Lib.h"
#import "JSONKit.h"

@interface LoginViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

//-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification;

@end

@implementation LoginViewController{
    NSMutableData *mutableData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *arrInputControl = @[txtEmail, txtPassword];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:arrInputControl]];
    [self.keyboardControls setDelegate:self];
    
    //set navigation bar
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], MPM_FONT_PRO_LIGHT(24), nil]];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self btnGo:nil];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    txtError.text = @"";
    [self.keyboardControls setActiveField:textField];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark end keyboard

#pragma mark -
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



// check email for registration
- (void)serviceLogin
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"email": [txtEmail.text lowercaseString], @"pwd":txtPassword.text, @"langId":[Lib localizedWithKey:@"MPM_LANG_ID"]};
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, LOGIN_EMAIL_SERVICE];
    
    NSString* serviceTitle = @"LOGIN BY EMAIL";
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    
    NSOperation *op=[manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                [Lib resetUserData];
                
                NSMutableDictionary* mDic = [NSMutableDictionary dictionaryWithDictionary:[results objectForKey:@"Data"]];
                [mDic setObject:[NSNumber numberWithBool:NO] forKey:MPM_USER_IS_LOGGED_IN_BY_FACEBOOK];
                [LCoreData addProfile:mDic];
                [LCoreData saveContext];
                [LCoreData clearAllMoodTimeline];
                NSNumber* mNumber = (NSNumber*)[results objectForKey:MPM_SERVICE_RESPONSE_CODE];
                
                BOOL isNeededToUpdateData = [mNumber intValue] == 102;
                if ([mNumber intValue] == 102) {
                    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:MPM_USER_NEEDED_TO_UPDATE_INFO];
                    
                    [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_WARNING_NEED_TO_UPDATE_PROFILE"]];
                    
                    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"editProfileView"];
                    [self.navigationController pushViewController:controller animated:YES];
                }else{
                    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:MPM_USER_NEEDED_TO_UPDATE_INFO];
                    
                    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"menuView"];
                    
                    [self.navigationController pushViewController:controller animated:YES];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isNeededToUpdateData] forKey:MPM_USER_NEEDED_TO_UPDATE_INFO];
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                txtError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            txtError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        txtError.text = [error localizedDescription];
        [Lib showAlertMessage:[error localizedDescription]];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}



- (IBAction)btnGo:(id)sender {
    txtError.text = @"";
    if (![txtEmail.text isEmail]) {
        txtError.text = [Lib localizedWithKey:@"MPM_KEY_ERROR_EMAIL_IS_INVALID"];
    }else if([txtPassword.text isNullOrEmpty] || [txtEmail.text isNullOrEmpty]){
        txtError.text = [Lib localizedWithKey:@"MPM_KEY_ERROR_ALL_MUST_NOT_BE_EMPTY"];
    }else{
        [self serviceLogin];
    }
}




- (IBAction)btnForgotPassword:(id)sender {
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"forgotPasswordView"];
    [self.navigationController pushViewController:controller animated:YES];
}


// get information from Facebook account
- (void)serviceGetInfoFaceBook:(NSString*)accToken
{
    if (accToken == nil || accToken.length < 1) {
        [Lib removeLoadingViewOn:self.view];
//        [Lib showAlertMessage:@"Facebook token is invalid"];
        [Lib logToFile:@"LOGIN BY FACEBOOK: Token is invalid"];
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"accToken": accToken, @"langId":[Lib localizedWithKey:@"MPM_LANG_ID"]};
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, LOGIN_FACEBOOK_SERVICE];
    NSString* serviceTitle = @"LOGIN BY FACEBOOK";
    
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    
    NSOperation* op= [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary* results = (NSDictionary *)responseObject;
        NSLog(@"%@ RESPONSE: %@", serviceTitle, results);
        if (results) {
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                [Lib resetUserData];
                
                NSMutableDictionary* mDic = [NSMutableDictionary dictionaryWithDictionary:[results objectForKey:@"Data"]];
                [mDic setObject:[NSNumber numberWithBool:YES] forKey:MPM_USER_IS_LOGGED_IN_BY_FACEBOOK];
                [LCoreData addProfile:mDic];
                [LCoreData saveContext];
                
                [LCoreData clearAllMoodTimeline];
                
                BOOL isNeededToUpdateData = [[results objectForKey:MPM_SERVICE_RESPONSE_CODE] intValue] == MPM_SERVICE_RESPONSE_CODE_NEEDED_UPDATE_DATA;
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isNeededToUpdateData] forKey:MPM_USER_NEEDED_TO_UPDATE_INFO];
                
                if (isNeededToUpdateData) [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_WARNING_NEED_TO_UPDATE_PROFILE"]];
                if (isNeededToUpdateData) {
                    
                    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"editProfileView"];
                    [self.navigationController pushViewController:controller animated:YES];
                }else{
                    
                    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"menuView"];
                    
                    [self.navigationController pushViewController:controller animated:YES];
                    
                }
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                txtError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            txtError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
        }
        
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        txtError.text = [error localizedDescription];
    }];
    
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}


// Create new account bases on exist facebook account.
- (IBAction)btnFacebook:(id)sender {
    [Lib showIndicatorViewOn2:self.view];
    
    if (![Lib checkInternetConnectionWithMessage]) {
        [Lib removeIndcatorViewOn:self.view];
        return;
    }
    
    // If the session state is any of the two "open" states when the button is clicked
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        // If the session state is not any of the two "open" states when the button is clicked
    }
    // Open a session showing the user the login UI
    // You must ALWAYS ask for public_profile permissions when opening a session
    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         
         // Retrieve the app delegate
         AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
         // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
         [appDelegate sessionStateChanged:session state:state error:error];
         if (error != nil) {
             
             [Lib removeIndcatorViewOn:self.view];
             [Lib logToFile:[NSString stringWithFormat:@"---- ERROR GET FACEBOOK TOKEN ----- %@",[ error localizedDescription]]];
             [Lib showAlertMessage:[NSString stringWithFormat:@"Authorize Facebook failed!\n %@", [error localizedDescription]]];
         }else{
             [self serviceGetInfoFaceBook:[session accessTokenData].accessToken];
         }
     }];

}


@end
