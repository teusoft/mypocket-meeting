//
//  HelpView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PopUpHelpView.h"

@interface HelpView : UIViewController<PopUpHelpViewDelegate>

@property (strong, nonatomic) IBOutlet UITextView *txtContent;

@property (strong, nonatomic) IBOutlet UILabel *lblHelpMood;
@property (strong, nonatomic) IBOutlet UILabel *lblVersion;

@property (strong, nonatomic) PopUpHelpView *popUpHelpView;

@end
