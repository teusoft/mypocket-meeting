//
//  MPMCalendarObject.h
//  MyPocket Meeting
//
//  Created by Nguyen Sy Tan on 12/11/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPMCalendarObject : NSObject

@property NSString* MPM_Title;
@property NSString* MPM_ID;
@property NSDate* MPM_StartDate;
@property NSTimeZone* MPM_TimeZone;

+ (MPMCalendarObject*)initWithDictionary:(NSDictionary*)dic;

@end
