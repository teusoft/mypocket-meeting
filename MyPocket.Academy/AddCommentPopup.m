//
//  AddCommentPopup.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/5/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "AddCommentPopup.h"
#import "LCoreData.h"
#import "AFNetworking.h"
#import "Lib.h"
#import "JSONKit.h"
#import "ServiceLib.h"
#import "Profile.h"

@interface AddCommentPopup ()

@end

@implementation AddCommentPopup

@synthesize timelineItem;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.7];
    addCommentView.layer.cornerRadius = 0;
    
    txtTextComment.text = timelineItem.note;
    
    [txtTextComment becomeFirstResponder];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (void)closePopup {
    [self removeAnimate];
}

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}


// get information from Facebook account
- (void)serviceAddComment
{
    [Lib showIndicatorViewOn2:self.view];
    
    Profile* profileObject = [LCoreData getProfile];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:MPM_DATE_TIME_FORMAT];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", SERVICE_URL, ADD_TIMELINE_SERVICE];
    
    NSDictionary *params = @{@"token":profileObject.userToken,
                             @"title":timelineItem.title,
                             @"moodBefore":timelineItem.moodBefore,
                             @"moodAfter":timelineItem.moodAfter,
                             @"dateTime":[dateFormatter stringFromDate:timelineItem.dateTime],
                             @"note":timelineItem.note,
                             @"moodId":timelineItem.moodId};
    
    NSString* serviceTitle = @"ADD COMMENT FOR MEETING";
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    
    NSOperation* op = [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Lib removeIndcatorViewOn:self.view];
        NSDictionary* results = (NSDictionary *)responseObject;
        NSLog(@"%@ RESPONSE: %@",serviceTitle, results);
        if (results) {
            
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_SUCCESS_MOOD_TIME_LINE_IS_UPDATED"]];
                timelineItem.isPosted = [NSNumber numberWithBool:YES];
                [LCoreData saveContext];
                [self.delegate addCommentPopupDidUpdateComment];
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE] ];
            }
            
            [self removeAnimate];
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            [Lib showAlertMessage: MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        [Lib showAlertMessage:[error localizedDescription]];
    }];
    
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
    
}


- (IBAction)btnOk:(id)sender {
    if ([txtTextComment.text isNullOrEmpty]) {
        [Lib showAlertMessage:FORMAT( [Lib localizedWithKey:@"MPM_FORMAT_COULD_NOT_BE_EMPTY"],@"Comment")];
        return;
    }
    
    timelineItem.note = txtTextComment.text;
    if ([Lib checkInternetConnection]) {
        [self serviceAddComment];
    }else{
        timelineItem.isPosted = [NSNumber numberWithBool:NO];
        [LCoreData saveContext];
        [self.delegate addCommentPopupDidUpdateComment];
        [self removeAnimate];
    }
}

- (IBAction)btnCancel:(id)sender {
    [self removeAnimate];
}

@end
