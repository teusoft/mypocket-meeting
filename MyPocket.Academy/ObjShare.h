//
//  ObjShare.h
//  ExpensesEntry
//
//  Created by Lex on 6/24/13.
//  Copyright (c) 2013 Lex. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface ObjShare : NSObject

@property (nonatomic, readwrite)  BOOL isOnMeeting;
@property (nonatomic, strong)  NSArray *arrColor;

@property (nonatomic, strong)  NSDateFormatter *dateFormat;
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

+ (ObjShare *)sharedInstance;

@end