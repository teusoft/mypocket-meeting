//
//  Profile.h
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 11/7/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Profile : NSManagedObject

@property (nonatomic, retain) NSString * userToken;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * avatar;
@property (nonatomic, retain) NSNumber * gender;
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * occupation;
@property (nonatomic, retain) NSString * lastLogin;
@property (nonatomic, retain) NSNumber * isFacebook;
@property (nonatomic, retain) NSNumber * isFirstLogin;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * meetingTimer;

@end
