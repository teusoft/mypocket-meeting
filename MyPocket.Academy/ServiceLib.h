//
//  ServiceLib.h
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/31/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#define POST_METHOD @"POST"
#define GET_METHOD @"GET"
#define PUT_METHOD @"PUT"
#define DELETE_METHOD @"DELETE"

#import <Foundation/Foundation.h>

@interface ServiceLib : NSObject {

}
@property (nonatomic, assign) BOOL testMode;
@property (nonatomic, assign) BOOL betaMode;

+(NSString*)removeLogsSoapResponse:(NSString*)strSoap;
+(NSString*)sendGetRequest:(NSString*)strURL withContent:(NSString*)strContent;
+(NSString*)sendRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL;
+(NSString*)sendRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL method:(NSString*)method;
+(NSString*)sendGetRequest:(NSString*)strURL;
+(NSString*)sendGetRequestWithHeader:(NSString*)strURL;
+(NSString*)sendGetRequestWithHeader:(NSString*)strURL andUserKey:(NSString*)key;
+(NSString*)sendGetRequestLatinEncoding:(NSString*)strURL;
+(ServiceLib*)shareInstance;
+(NSString*)stringByRemoveDebug:(NSString*)str;
+(NSString*)postMultiPartRequest:(NSMutableArray*)params
                           files:(NSMutableArray*)files
                          andUrl:(NSString*)strURL;

+(NSString*)sendJsonRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL method:(NSString*)method;
+(NSString*)sendJsonRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL andUserKey:(NSString*)key method:(NSString*)method;

@end
