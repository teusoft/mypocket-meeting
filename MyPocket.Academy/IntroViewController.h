//
//  IntroViewController.h
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController<UIScrollViewDelegate> {
        
    __weak IBOutlet UIScrollView *scrollContent;
    __weak IBOutlet UIPageControl *pageControl;
    
    BOOL pageControlBeingUsed;

    int pageCurrent;
}

@end
