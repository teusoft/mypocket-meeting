//
//  MyProfileView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//


#import "MyProfileView.h"




@interface MyProfileView ()

@end

@implementation MyProfileView {
    Profile *profileObject;
    NSArray *arrMoodTimeline;
    
    BOOL isGoogleAuthorized;
    
    NSTimer* timerForCloudCalendar;
    
    int notificationIndex;
    NSMutableArray* notificationArray;
    
    BOOL shouldPauseTimer;
}


@synthesize operationQueue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    mScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 1250);
    listSubViews= [[NSMutableArray alloc] init];
    isGoogleAuthorized = NO;
    currentPage = 1;
    
    profileObject = [LCoreData getProfile];
    
    // Track User email
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    [mixpanel identify:profileObject.email];
    
    arrMoodTimeline = [LCoreData getAllMoodTimeline];
    isOnMeeting = NO;
    
    
    isLoadingMoreData = NO;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:MPM_TIME_DATE_FORMAT];
    
    
    notificationIndex = 0;
    notificationArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NOTIFICATION_SHOW_INCOMING_EVENT object:nil queue:nil usingBlock:^(NSNotification *note) {
        if (note.userInfo) {
            NSDate* mDate = (NSDate*)[note.userInfo objectForKey:NOTIFICATION_DATE];
            int minute = ceil([mDate timeIntervalSinceNow] / 60);
            UIAlertView* mAlert = [[UIAlertView alloc] initWithTitle:MPM_ALERT_TITLE message:FORMAT([Lib localizedWithKey:@"MPM_ALERT_ASK_TO_RATE"],[note.userInfo objectForKey:NOTIFICATION_TITLE], minute) delegate:self cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_SKIP"] otherButtonTitles:[Lib localizedWithKey:@"MPM_KEY_OK"], nil];
            mAlert.tag = 20000+notificationIndex ;
            [notificationArray addObject:note.userInfo];
            [self.tabBarController setSelectedIndex:0];
            [mAlert show];
            notificationIndex++;
        }
        
       
    }];
    
  
    [[NSNotificationCenter defaultCenter] addObserverForName:NOTIFICATION_NETWORK_STATUS object:nil queue:nil usingBlock:^(NSNotification *note) {
        if ([Lib checkInternetConnection]) {
            NSLog(@"online");
            
            lbNetworkStatus.text =  [Lib localizedWithKey:@"MPM_KEY_INTERNET_AVAILABLE"];
            lbNetworkStatus.textColor = [UIColor colorWithRed:16/255.f green:164/255.f blue:52/255.f alpha:1.f];
            
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
            [animation setFromValue:[NSNumber numberWithFloat:1.0]];
            [animation setToValue:[NSNumber numberWithFloat:0.0]];
            [animation setDuration:0.35f];
            [animation setTimingFunction:[CAMediaTimingFunction
                                          functionWithName:kCAMediaTimingFunctionLinear]];
            [animation setAutoreverses:YES];
            [animation setRepeatCount:3];
            [[lbNetworkStatus layer] addAnimation:animation forKey:@"opacity"];
            
            lbNetworkStatus.alpha = 0.f;
            
            [self syncAllData];
            
        }else{
            NSLog(@"offline");
            lbNetworkStatus.text = [Lib localizedWithKey:@"MPM_KEY_NO_INTERNET_CONNECTION"];
            lbNetworkStatus.textColor = [UIColor blackColor];
            
            
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
            [animation setFromValue:[NSNumber numberWithFloat:0.0]];
            [animation setToValue:[NSNumber numberWithFloat:1.0]];
            [animation setDuration:0.35f];
            [animation setTimingFunction:[CAMediaTimingFunction
                                          functionWithName:kCAMediaTimingFunctionLinear]];
            [animation setAutoreverses:YES];
            [animation setRepeatCount:2];
            [[lbNetworkStatus layer] addAnimation:animation forKey:@"opacity"];
            
            lbNetworkStatus.alpha = 1.f;
        }
    }];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_NEEDED_TO_SHOW_INSTRUCTION] boolValue]) {
        introView = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroViewController"];
        introView.view.alpha = 0.f;
        
        [self.tabBarController.view addSubview:introView.view];
        
        [UIView animateWithDuration:.25 animations:^{
            introView.view.alpha = 1.f;
        }];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:MPM_USER_NEEDED_TO_SHOW_INSTRUCTION];
    }
    
    

    timerForCloudCalendar = [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(btnRefreshEventClicked:) userInfo:nil repeats:YES];
    
    [self loadTimeline];
    [self addCreateNewEventView];
    
    [self addPreviousSmileyView];
    
  
}


- (void)serviceLoadTimeLineEvents
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{
                             @"token": profileObject.userToken,
                             @"page":[NSNumber numberWithInt:currentPage]
                             };
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, GET_TIMELINE_SERVICE];
    
    NSString* serviceTitle = @"LOAD TIMELINE EVENT";
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    NSOperation* op = [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                NSArray* feeds = (NSArray *)[results objectForKey:MPM_SERVICE_RESPONSE_DATA];
                
                if (currentPage==1) {
                    [LCoreData clearOnlineMoodTimeline];
                }
                
                for (NSDictionary *moodItem in feeds) {
                    [LCoreData addMoodTimeline:moodItem];
                }
                
                arrMoodTimeline = [LCoreData getAllMoodTimeline];
                
                [tblTimeline reloadData];
                
                currentPage++;
                [self initSubViewsLayout];

                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
            }else{
                [Lib logResponseWithName:serviceTitle response: [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            
        }
     [Lib removeIndcatorViewOn:self.view];
     
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response: [error localizedDescription]];
        [Lib showAlertMessage: [error localizedDescription]];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}

- (void)loadTimeline{
    [self serviceLoadTimeLineEvents];
}

-(void)syncAllData{
    
    countDone = 0;
    isSyncDone = NO;
    
    NSArray *arrOfflineMood = [LCoreData getLocalMoodTimeline];
    
    totalLocalMood = (int)arrOfflineMood.count;
    
    if (!totalLocalMood) {
        return;
    }
    
    
    lbNetworkStatus.alpha = 1.f;
    lbNetworkStatus.textColor = [UIColor blackColor];
    
    // Create a new NSOperationQueue instance.
    operationQueue = [NSOperationQueue new];
    
    operationQueue.maxConcurrentOperationCount = 1;
    
    [operationQueue addObserver: self forKeyPath: @"operations" options: NSKeyValueObservingOptionNew context: NULL];
    
    // Create a new NSOperation object using the NSInvocationOperation subclass.
    // Tell it to run the counterTask method.
    
    
    
    for (MoodTimeline *moodItem in arrOfflineMood) {
        NSInvocationOperation *operation1 = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                 selector:@selector(servicePostMoodTimeLine:)
                                                                                   object:moodItem];
        // Add the operation to the queue and let it to be executed.
        [operationQueue addOperation:operation1];
    }
    
}

// check email for registration
- (void)servicePostMoodTimeLine:(MoodTimeline*)moodItem
{
    isSyncDone = NO;
    [self setLoadingText:[NSString stringWithFormat:[Lib localizedWithKey:@"MPM_KEY_POST_MOOD"], countDone+1, totalLocalMood]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:MPM_DATE_TIME_FORMAT];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *serviceURL = [NSString stringWithFormat:@"%@%@", SERVICE_URL, ADD_TIMELINE_SERVICE];
    
    NSDictionary *params = @{@"token":profileObject.userToken,
                            @"title":moodItem.title,
                            @"moodBefore":moodItem.moodBefore,
                            @"moodAfter":moodItem.moodAfter,
                            @"dateTime":[dateFormatter stringFromDate:moodItem.dateTime],
                            @"note":moodItem.note,
                             @"moodId":moodItem.moodId};
    
    
    
    NSString* serviceTitle = @"POST MOOD TIMELINE";
    [Lib logRequestWithName:serviceTitle url:serviceURL params:params];
    
    
    NSOperation *op=[manager POST:serviceURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                countDone++;
                [LCoreData deleteMoodTimeline:moodItem];
                
                [self setLoadingText:[NSString stringWithFormat:[Lib localizedWithKey:@"MPM_KEY_POST_MOOD_NUMBER_DONE"], countDone, totalLocalMood]];
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                isSyncDone = countDone == totalLocalMood;
                if (isSyncDone) {
                    [lbNetworkStatus willDismissAfterSeconds:3];
                    currentPage = 1;
                    [self loadTimeline];
                }
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}



- (void)setLoadingText:(NSString*)text{
    [lbNetworkStatus performSelectorOnMainThread:@selector(setText:)
                                      withObject:text
                                   waitUntilDone:YES];
}

- (void) observeValueForKeyPath:(NSString *) keyPath
                       ofObject:(id) object
                         change:(NSDictionary *) change
                        context:(void *) context {
    
    if (
        object == operationQueue
        &&
        [@"operations" isEqual: keyPath]
        ) {
        NSArray *operations = [change objectForKey:NSKeyValueChangeNewKey];
        
        if ( [self hasActiveOperations: operations] ) {
            NSLog(@"Loading");
        } else {
            NSLog(@"Done %d", countDone);
            if (countDone==totalLocalMood) {
                if (isSyncDone) {
                    NSLog(@"Done All");
                    [self setLoadingText:[Lib localizedWithKey:@"MPM_KEY_SYNC_DONE"]];
                    [NSThread sleepForTimeInterval:0.75];
                    // reload after successfull
                    
                }else{
                    NSLog(@"Error some");
                    // reload after error
                }
                
                [self performSelectorOnMainThread:@selector(didSyncData) withObject:self waitUntilDone:YES];
            }
        }
    }
}

- (void)didSyncData{
    lbNetworkStatus.alpha = 0.f;
    
    [self loadTimeline];
}

- (BOOL) hasActiveOperations:(NSArray *) operations {
    for ( id operation in operations ) {
        if ( [operation isExecuting] && ! [operation isCancelled] ) {
            return YES;
        }
    }
    
    return NO;
}


- (void)viewWillDisappear:(BOOL)animated
{
    shouldPauseTimer = YES;
    [Lib cleanSmileyViews];
    for (int i = 0; i < listSubViews.count; i++) {
        SmileyView* mView = (SmileyView*)[listSubViews objectAtIndex:i];
        
        if ([mView isKindOfClass:[SmileyView class]] && !mView.isRemoved && mView.shouldBeRemoved) {
            [Lib addSmileyViewWithTitle:[mView getTitle] startDate:[mView getDate] tag:[NSNumber numberWithInteger:mView.tag ]];
        }
    }
    
    [Lib setBadgeNumber];
}


- (void)viewWillAppear:(BOOL)animated{
    
    shouldPauseTimer = NO;
    
    
    [self btnRefreshEventClicked:nil];
    
    
    
    UIImage *img = [UIImage imageNamed:@"icon_profile"];
    UIImage *imgSelected = [UIImage imageNamed:@"icon_profileSelected"];
    
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imgSelected = [imgSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    self.tabBarItem.image = img;
    self.tabBarItem.selectedImage = imgSelected;
    
    cloudCalendarIcon.hidden = NO;
    if (selectedCalendar == MPM_CLOUD_CALENDAR_GOOGLE) {
        cloudCalendarIcon.image = [UIImage imageNamed:@"google_calendar_icon"];
    }else if(selectedCalendar == MPM_CLOUD_CALENDAR_ICLOUD){
        cloudCalendarIcon.image = [UIImage imageNamed:@"icloud_icon"];
    }else{
        cloudCalendarIcon.hidden = YES;
    }
 
    [self checkShouldRemoveSmileyView];
    
//    [self checkIsNeedToNextEvent];
}



#pragma mark - Button Actions



- (IBAction)btnSkipClick:(id)sender {
    if (_eventsList== nil || _eventsList.count <1) {
        [Lib showAlert:nil withMessage:[Lib localizedWithKey:@"MPM_KEY_NO_COMING_EVENT"]];
        return;
    }
    
    UIAlertView* mAlert = [[UIAlertView alloc] initWithTitle:MPM_ALERT_TITLE message:[Lib localizedWithKey:@"MPM_ALERT_ASK_TO_SKIP"] delegate:self cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_CANCEL"] otherButtonTitles:[Lib localizedWithKey:@"MPM_KEY_OK"], nil];
    mAlert.tag = 10010;
    [mAlert show];
}



#pragma mark -Mood Button Actions



- (void)serviceAddMoodTimeLine:(MoodTimeline*)moodTimeLine
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:MPM_DATE_TIME_FORMAT];
    
    NSDictionary *params = @{@"token":profileObject.userToken,
                             @"title":moodTimeLine.title,
                             @"moodBefore":moodTimeLine.moodBefore,
                             @"moodAfter":moodTimeLine.moodAfter,
                             @"dateTime":[dateFormatter stringFromDate:moodTimeLine.dateTime],
                             @"note":@"",
                             @"moodId":[NSNumber numberWithInt:0]};
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, ADD_TIMELINE_SERVICE];
    
    NSString* serviceTitle = @"ADD TIMELINE EVENT";
    
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    NSOperation *op= [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                moodTimeLine.isPosted = [NSNumber numberWithBool:YES];
                [LCoreData saveContext];

                // Track User Pushed Mood After
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                
                [mixpanel track:MPM_MIX_PANEL_KEY_PUSH_MOOD_AFTER properties:@{
                                                                               @"Value": moodTimeLine.moodAfter,
                                                                               }];
                
                
                currentPage = 1;
                [self loadTimeline];
                
                
                [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_SUCCESS_YOUR_PROFILE_IS_UPDATED"]];
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                
//                [self initViewForSmileys:0];
                
            }else{
                moodTimeLine.isPosted = [NSNumber numberWithBool:NO];
                [LCoreData saveContext];
                arrMoodTimeline = [LCoreData getAllMoodTimeline];
                
                [tblTimeline reloadData];
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
        }
        
        
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        [Lib showAlertMessage: [error localizedDescription]];
    }];
    
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}






#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoodTimeline *m = arrMoodTimeline[indexPath.row];
    
    CGSize sizeText = [self sizeOfMultiLineLabel:m.note];
    CGFloat rowHeight = sizeText.height+ 80;
    return rowHeight;
}

-(CGSize)sizeOfMultiLineLabel:(NSString *)text{
    
    NSAssert(self, @"UILabel was nil");
    NSString *aLabelTextString = text;
    UIFont *aLabelFont = MPM_FONT_EXTRA_LIGHT(12);
    CGFloat aLabelSizeWidth = tblTimeline.frame.size.width - 40;
    CGSize mSize = [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{
                                                            NSFontAttributeName : aLabelFont
                                                            }
                                                  context:nil].size;
    
    return mSize;
}
- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setAttributedText:text];
    CGSize size = [calculationView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return arrMoodTimeline.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MoodCell";
    
    MoodCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MoodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    MoodTimeline *moodTimeline = [arrMoodTimeline objectAtIndex:indexPath.row];
    
    [cell setDetailWithData:moodTimeline];
    
    cell.btnComment.tag = indexPath.row;
    [cell.btnComment addTarget:self action:@selector(btnAddComment:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;

}

#pragma mark - EventDetailViewControllerDelegate

- (void)skipEvent
{
    [self btnSkipClick:nil];
}

- (void)eventDetailDidTapToRateButtonForEvent:(MPMCalendarObject *)currentEvent
{
    int randNum = [[NSDate date] timeIntervalSince1970];
    SmileyView* mView = [SmileyView initWithPoint:CGPointMake(0, -240)];
    mView.tag = 100000000+randNum;
    mView.delegate = self;
    
    if (currentEvent) {
        [mView setMeetingTitle:currentEvent.MPM_Title startAt:currentEvent.MPM_StartDate];
        [listSubViews insertObject:mView atIndex:0];
        
        
        [mScrollView addSubview:mView];
        
        [self initSubViewsLayout];
    }
    [self gotoNextEvent];
}

#pragma mark- UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height+60)
    {
        if (!isLoadingMoreData)
        {
            if (![Lib checkInternetConnection]) {
                return;
            }
            
            if (arrMoodTimeline.count>=5) {
                isLoadingMoreData = YES;
                [self loadTimeline];
            }
        }
    }
  
}

#pragma mark- IBAction - UI Event Handler ---


- (IBAction)btnEventDetailClick:(id)sender {
    
    if (_eventsList == nil || _eventsList.count < 1) {
        return;
    }
    _eventDetailPopup = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
    MPMCalendarObject *eventItem = (MPMCalendarObject*)[_eventsList objectAtIndex:0];
    
    _eventDetailPopup.currentEvent = eventItem;
    _eventDetailPopup.delegate = self;
    [_eventDetailPopup showPopUp:self.view animated:YES];
}


- (IBAction)btnAddComment:(id)sender {
    
    _addCommentPopup = [self.storyboard instantiateViewControllerWithIdentifier:@"addCommentPopup"];
    _addCommentPopup.timelineItem = arrMoodTimeline[[sender tag]];
    
    _addCommentPopup.delegate = self;
    [_addCommentPopup showPopUp:self.view animated:YES];
}

#pragma mark- AddCommentPopupDelegate


- (void)addCommentPopupDidUpdateComment{
    currentPage = 1;
    if ([Lib checkInternetConnection]) {
        
        [self loadTimeline];
    }else{
        arrMoodTimeline = [LCoreData getAllMoodTimeline];
        [tblTimeline reloadData];
    }
}



#pragma mark -
#pragma mark Access Calendar iCloud

// Check the authorization status of our application for Calendar
-(void)checkEventStoreAccessForCalendar
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status)
    {
            // Update our UI if the user has granted access to their Calendar
        case EKAuthorizationStatusAuthorized: [self accessGrantedForCalendar];
            break;
            // Prompt the user for access to Calendar if there is no definitive answer
        case EKAuthorizationStatusNotDetermined: [self requestCalendarAccess];
            break;
            // Display a message if the user has denied or restricted access to Calendar
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Lib localizedWithKey:@"MPM_WARNING_PRIVACY"] message:[Lib localizedWithKey:@"MPM_WARNING_DETAIL"]
                                                           delegate:nil
                                                  cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_OK"]
                                                  otherButtonTitles:nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}


// Prompt the user for access to their Calendar
-(void)requestCalendarAccess
{
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             // Let's ensure that our code will be executed from the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // The user has granted access to their Calendar; let's populate our UI with all events occuring in the next 24 hours.
                 [self accessGrantedForCalendar];
             });
         }
     }];
}


// This method is called when the user has granted permission to Calendar
-(void)accessGrantedForCalendar
{
    if (selectedCalendar != MPM_CLOUD_CALENDAR_ICLOUD) {
        return;
    }
    // Let's get the default calendar associated with our event store
    self.defaultCalendar = self.eventStore.defaultCalendarForNewEvents;
    if (_eventsList) {
        [_eventsList removeAllObjects];
    }else{
        _eventsList = [[NSMutableArray alloc] init];
    }

    [Lib setLastEventDate];
    NSDate* mLastDate = [Lib getLastEventDate];
    
//    NSDate* mLastDate = [NSDate dateWithTimeInterval:-2*24*60*60 sinceDate:[NSDate date]];
    NSLog(@"=============START %@ =================", mLastDate);
    for (EKEvent *objEvent in [self fetchEvents]) {
        
        MPMCalendarObject* mObj = [MPMCalendarObject new];
        mObj.MPM_ID = objEvent.eventIdentifier;
        mObj.MPM_Title = objEvent.title;
        mObj.MPM_StartDate = objEvent.startDate;
        mObj.MPM_TimeZone = objEvent.timeZone;
//        NSLog(@"---------------");
//        NSLog(@"%@", mObj.MPM_ID);
//        NSLog(@"%@", mObj.MPM_Title);
//        NSLog(@"%@", mObj.MPM_StartDate);
//        NSLog(@"---========----");
        if ([mObj.MPM_StartDate compareToDate:mLastDate] > 0) {
            [_eventsList addObject:mObj];
            NSLog(@" |======= ADD %@ =========", mObj.MPM_Title);
        }
        
        
    }
    
    
    NSLog(@"=============END===============");
    [self scheduleShowIncomingEventNotification];
    
    [self loadFirstEvent];
    
    [self finishLoadingCalendar];
}


// Fetch all events happening in the next 7 days
- (NSMutableArray *)fetchEvents
{
    NSDate* mLastDate = [Lib getLastEventDate];
    
//    NSDate* mLastDate = [NSDate dateWithTimeInterval:-2*24*60*60 sinceDate:[NSDate date]];
    
    NSDateComponents *tomorrowDateComponents = [[NSDateComponents alloc] init];
    tomorrowDateComponents.day = 10;
    NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:tomorrowDateComponents
                                                                    toDate:mLastDate
                                                                   options:0];
    // We will only search the default calendar for our events
    NSArray *calendarArray = [NSArray arrayWithObject:self.defaultCalendar];
    
    // Create the predicate
    NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:mLastDate
                                                                      endDate:endDate
                                                                    calendars:calendarArray];
    
    // Fetch all events that match the predicate
    NSMutableArray *events = [NSMutableArray arrayWithArray:[self.eventStore eventsMatchingPredicate:predicate]];
    
    return events;
}


#pragma mark - GoogleOAuth class delegate method implementation

-(void)authorizationWasSuccessful{
    
    [Lib removeIndcatorViewOn:self.view];
    isGoogleAuthorized = YES;
    
    [_googleOAuth callAPI:@"https://www.googleapis.com/calendar/v3/users/me/calendarList"
           withHttpMethod:httpMethod_GET
       postParameterNames:nil
      postParameterValues:nil];
}


-(void)responseFromServiceWasReceived:(NSString *)responseJSONAsString andResponseJSONAsData:(NSData *)responseJSONAsData{
    NSError *error;
    [Lib logToFile:@"---- Response Google Calendar ----"];
//    [Lib logToFile:responseJSONAsString];
    
    [Lib removeIndcatorViewOn:self.view];
    
    //    NSLog(@"responseJSONAsString: %@", responseJSONAsString);
    
    if ([responseJSONAsString rangeOfString:@"calendarList"].location != NSNotFound) {
        [Lib logToFile:@"---- Load Google Calendar List ----"];
        
        // Get the JSON data as a dictionary.
        NSDictionary *calendarInfoDict = [NSJSONSerialization JSONObjectWithData:responseJSONAsData options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            // This is the case that an error occured during converting JSON data to dictionary.
            // Simply log the error description.
            NSLog(@"%@", [error localizedDescription]);
        }
        else{
            // Get the calendars info as an array.
            NSArray *calendarsInfo = [calendarInfoDict objectForKey:@"items"];
            
            // If the arrGoogleCalendars array is nil then initialize it so to store each calendar as a NSDictionary object.
            if (_arrGoogleCalendars == nil) {
                _arrGoogleCalendars = [[NSMutableArray alloc] init];
            }
            [_arrGoogleCalendars removeAllObjects];
            // Make a loop and get the next data of each calendar.
            for (int i=0; i<[calendarsInfo count]; i++) {
                // Store each calendar in a temporary dictionary.
                NSDictionary *currentCalDict = [calendarsInfo objectAtIndex:i];
                
                
                // Create an array which contains only the desired data.
                NSArray *values = [NSArray arrayWithObjects:[currentCalDict objectForKey:@"id"],
                                   [currentCalDict objectForKey:@"summary"],
                                   nil];
                // Create an array with keys regarding the values on the previous array.
                NSArray *keys = [NSArray arrayWithObjects:@"id", @"summary", nil];
                
                // Add key-value pairs in a dictionary and then add this dictionary into the arrGoogleCalendars array.
                [_arrGoogleCalendars addObject:
                 [[NSMutableDictionary alloc] initWithObjects:values forKeys:keys]];
            }
            
            // load Events list
            NSDictionary *dictCurrentCalendar = _arrGoogleCalendars[0];
            
            NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/calendar/v3/calendars/%@/events", [dictCurrentCalendar objectForKey:@"id"]];
            
            NSLog(@"url: %@", url);
            
            [_googleOAuth callAPI:url
                   withHttpMethod:httpMethod_GET
               postParameterNames:nil
              postParameterValues:nil];
            
        }
    }
    else if ([responseJSONAsString rangeOfString:@"calendar#event"].location != NSNotFound){
        // If the Google response contains the "calendar#event" literal then the event has been added to the selected calendar
        // and Google returns data related to the new event.
        
        [Lib logToFile:@"---- Load Google Event List ----"];
        
        if (_eventsList) {
            [_eventsList removeAllObjects];
        }else{
            _eventsList = [[NSMutableArray alloc] init];
        }
        
        // Get the response JSON as a dictionary.
        id eventInfoDict = [responseJSONAsString objectFromJSONString];
        
        
        
        NSLog(@"Events List: %@", eventInfoDict);
        if (eventInfoDict) {
            
            
            
            NSDate* mLastDate = [Lib getLastEventDate];
            
            NSLog(@"=============START %@ =================", mLastDate);
            for (NSDictionary *eventItem in eventInfoDict[@"items"]) {
                MPMCalendarObject* mObj = [MPMCalendarObject initWithDictionary:eventItem];
                
                NSLog(@"---------------");
                NSLog(@"%@", mObj.MPM_ID);
                NSLog(@"%@", mObj.MPM_Title);
                NSLog(@"%@", mObj.MPM_StartDate);
                NSLog(@"---========----");
                if ([mObj.MPM_StartDate compareToDate:mLastDate] > 0) {
                    [_eventsList addObject:mObj];
                    NSLog(@" ======= ADD %@ =========", mObj.MPM_Title);
                }
            }
            
            NSLog(@"=============END===============");
            
        }
        [self scheduleShowIncomingEventNotification];
        [self loadFirstEvent];
        [Lib removeIndcatorViewOn:self.view];
        [self finishLoadingCalendar];
    }
}


-(void)accessTokenWasRevoked{
    // Remove all calendars from the array.
    [_arrGoogleCalendars removeAllObjects];
    _arrGoogleCalendars = nil;
    
    isLoadingCalendar = NO;
    [Lib removeIndcatorViewOn:self.view];
}


-(void)errorOccuredWithShortDescription:(NSString *)errorShortDescription andErrorDetails:(NSString *)errorDetails{
    // Just log the error messages.
    NSLog(@"%@", errorShortDescription);
    NSLog(@"%@", errorDetails);
    
    isLoadingCalendar = NO;
    [Lib removeIndcatorViewOn:self.view];
}


-(void)errorInResponseWithBody:(NSString *)errorMessage{
    // Just log the error message.
    NSLog(@"%@", errorMessage);
    
    isLoadingCalendar = NO;
    [Lib removeIndcatorViewOn:self.view];
}


#pragma - mark Loading Cloud Calendar --------

- (void)gotoNextEvent
{
    if (_eventsList == nil || _eventsList.count < 1) {
        _lblTextTimer.text = [Lib localizedWithKey:@"MPM_KEY_NO_COMING_EVENT"];
        btnSkip.hidden = YES;
        return;
    }
    MPMCalendarObject *mObj = (MPMCalendarObject*)[_eventsList objectAtIndex:0];
    
    [Lib setLastEventDate:mObj.MPM_StartDate];
    [Lib removeNotification:mObj.MPM_ID];
    NSLog(@"======== NEW LASTDATE %@", mObj.MPM_StartDate);
    [_eventsList removeObjectAtIndex:0];
    [self loadFirstEvent];
}

- (void)loadFirstEvent
{
    if (selectedCalendar == MPM_CLOUD_CALENDAR_NONE) {
        return;
    }
    
    
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = 0.5f;
    transition.type =  @"cube";
    transition.subtype = @"fromTop";
    [_lblTextTimer.layer removeAllAnimations];
    [_lblTextTimer.layer addAnimation:transition forKey:kCATransition];
    
    btnSkip.hidden = NO;
    
    if (_eventsList.count < 1) {
        _lblTextTimer.text = [Lib localizedWithKey:@"MPM_KEY_NO_COMING_EVENT"];
        btnSkip.hidden = YES;
        return;
    }
    MPMCalendarObject *mObj = (MPMCalendarObject*)[self.eventsList objectAtIndex:0];
    
    _lblTextTimer.text = [NSString stringWithFormat:[Lib localizedWithKey:@"MPM_KEY_AT"], [dateFormat stringFromDate:mObj.MPM_StartDate]];
    
    btnSkip.hidden = NO;
}

/*
- (void)loadNextEvent
{
    
    if (selectedCalendar == MPM_CLOUD_CALENDAR_NONE) {
        return;
    }
    
    currentIndex ++;
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = 0.5f;
    transition.type =  @"cube";
    transition.subtype = @"fromTop";
    [_lblTextTimer.layer removeAllAnimations];
    [_lblTextTimer.layer addAnimation:transition forKey:kCATransition];
    
    btnSkip.hidden = NO;
    
    if (currentIndex < 0 || _eventsList.count < 1) {
        _lblTextTimer.text = [Lib localizedWithKey:@"MPM_KEY_NO_COMING_EVENT"];
        btnSkip.hidden = YES;
        return;
    }
    
    if (currentIndex > 0 && currentIndex <= _eventsList.count) {
        MPMCalendarObject *mObj = (MPMCalendarObject*)self.eventsList[currentIndex-1];
        
        [Lib setLastEventDate:mObj.MPM_StartDate];
        
        [Lib removeNotification:mObj.MPM_ID];
        
        NSLog(@"======== NEW LASTDATE %@", mObj.MPM_StartDate);
    }
    
    
    if (currentIndex <= _eventsList.count-1) {
        MPMCalendarObject *mObj = (MPMCalendarObject*)self.eventsList[currentIndex];
        _lblTextTimer.text = [NSString stringWithFormat:@"AT %@", [dateFormat stringFromDate:mObj.MPM_StartDate]];
        
        
        btnSkip.hidden = NO;
    }else{
        _lblTextTimer.text = [Lib localizedWithKey:@"MPM_KEY_NO_COMING_EVENT"];
        btnSkip.hidden = YES;
    }

}

 */
 
- (NSDate*)estimateNotificationDate:(NSDate*)mDate
{
    if ([mDate compareToDate:[NSDate date]] < 0) {
        return nil;
    }
    NSDate* temDate =[NSDate dateWithTimeInterval:-15*60 sinceDate:mDate];
    
    if ([temDate compareToDate:[NSDate date]] >= 0) {
        return temDate;
    }
    return [NSDate date];
}

- (void)doLoadCalendar{
    [Lib logToFile:@"---- Loading Calendar ----"];
    
    
    switch (selectedCalendar) {
        case 1: {
            // Initialize the event store
            _eventStore = [[EKEventStore alloc] init];
            // Initialize the events list
            _eventsList = nil;
            _eventsList = [[NSMutableArray alloc] init];
            
            [self checkEventStoreAccessForCalendar];
            
            [_googleOAuth revokeAccessToken];
            
            break;
        }
            
        case 2:{
            
            [_eventStore reset];
            [_eventsList removeAllObjects];
            if (_googleOAuth == nil || !isGoogleAuthorized) {
                _googleOAuth = [[GoogleOAuth alloc] initWithFrame:[[[UIApplication sharedApplication] delegate] window].frame];
                // Set self as the delegate.
                [_googleOAuth setGOAuthDelegate:self];
                
                [Lib showIndicatorViewOn2:self.view];
                
                [Lib logToFile:@"---- Loading  Google Calendar ----"];
                [_googleOAuth authorizeUserWithClienID:GOOGLE_CLIENT_ID
                                       andClientSecret:GOOGLE_CLIENT_SECRET
                                         andParentView:[[[UIApplication sharedApplication] delegate] window]
                                             andScopes:[NSArray arrayWithObject:@"https://www.googleapis.com/auth/calendar"]];
            }else if (isGoogleAuthorized) {
                
                [_googleOAuth callAPI:@"https://www.googleapis.com/calendar/v3/users/me/calendarList"
                       withHttpMethod:httpMethod_GET
                   postParameterNames:nil
                  postParameterValues:nil];
            }
            break;
        }
            
        default:
            break;
    }
    
}



- (void)btnRefreshEventClicked:(id)sender
{
    selectedCalendar = [Lib getCloudCalendarMode];
    if (!shouldPauseTimer && !isLoadingCalendar && selectedCalendar != MPM_CLOUD_CALENDAR_NONE) {
        if (selectedCalendar == MPM_CLOUD_CALENDAR_GOOGLE && ![Lib checkInternetConnection]) {
            return;
        }
        [_eventsList removeAllObjects];
        isLoadingCalendar = YES;
        cloudCalendarIcon.hidden = YES;
        [loadingIndicator startAnimating];
        [self doLoadCalendar];
    }else{
        
        [_eventsList removeAllObjects];
        isLoadingCalendar = NO;
        cloudCalendarIcon.hidden = YES;
        [loadingIndicator stopAnimating];
        _lblTextTimer.text = [Lib localizedWithKey:@"MPM_KEY_NO_COMING_EVENT"];
        btnSkip.hidden = YES;
        
    }
}

- (void)finishLoadingCalendar
{
    
    isLoadingCalendar = NO;
    cloudCalendarIcon.hidden = selectedCalendar == MPM_CLOUD_CALENDAR_NONE;
    [loadingIndicator stopAnimating];
}


#pragma - mark Smiley Table View Cell Delegate

- (void)smileyViewTableViewCell:(SmileyView *)cell didFinishWithMoodTimeLine:(MoodTimeline *)moodEvent
{
    if ([Lib checkInternetConnectionWithMessage]) {
        [Lib showIndicatorViewOn2:self.view];
        [self serviceAddMoodTimeLine:moodEvent];
    }else{
        arrMoodTimeline = [LCoreData getAllMoodTimeline];
        [tblTimeline reloadData];
    }
    
}


- (void)smileyViewTableViewCellDidTimeOut:(SmileyView *)cell
{
//    UILocalNotification *notification = [[UILocalNotification alloc]init];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    [notification setAlertBody:[NSString stringWithFormat:@"Finish event: %@", currentMeeting.title]];
//    [notification setFireDate:[NSDate dateWithTimeIntervalSinceNow:1.f]];
//    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"FINISH_EVENT" forKey:@"notif"];
//    notification.userInfo = infoDict;
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

- (void)smileyViewTableViewCellDidTapTimer:(SmileyView *)cell
{
    
    _slideTimerView = [self.storyboard instantiateViewControllerWithIdentifier:@"slideTimerView"];
    _slideTimerView.delegate = self;
    [_slideTimerView showPopUp:self.view animated:YES withTag:(int)cell.tag];
}


- (void)smileyViewTableViewCellDidTapTips:(SmileyView *)cell withId:(int)tipId
{
    NSLog(@"TIP ID = %i", tipId);
    tipView = [self.storyboard instantiateViewControllerWithIdentifier:@"QuickTipsView"];
    tipView.view.alpha = 0.f;
    [tipView loadTipForMoodId:tipId];
    
    [self.view addSubview:tipView.view];
    
    [UIView animateWithDuration:.25 animations:^{
        tipView.view.alpha = 1.f;
    }];

}

- (void)smileyViewTableViewCellDidCancel:(SmileyView *)cell
{
    if (cell.shouldBeRemoved) {
        [UIView animateWithDuration:0.3 animations:^{
            cell.alpha = 0.0;
        } completion:^(BOOL finished) {
            if (finished) {
                cell.isRemoved = YES;
                [self initSubViewsLayout];
                [cell removeFromSuperview];
            }
        }];
    }else{
        [self initSubViewsLayout];
    }
}

- (void)smileyViewTableViewCell:(SmileyView *)cell didStartWithMoodIndex:(int)moodId
{
    // Track User Pushed Mood After
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    [mixpanel track:MPM_MIX_PANEL_KEY_PUSH_MOOD_BEFORE properties:@{
                                                                   @"Value": [NSNumber numberWithInt:moodId]
                                                                   }];

    [self initSubViewsLayout];
}

- (void)smileyViewTableViewCellDidGoToFinishStep:(SmileyView *)cell
{
    
}

- (void)smileyViewTableViewCellDidTapShowDetail:(SmileyView *)cell withTitle:(NSString *)title atDate:(NSDate *)mDate
{
    _eventDetailPopup = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
    
    
    _eventDetailPopup.delegate = self;
    [_eventDetailPopup showPopUpWithTitle:title atDate:mDate inView:self.view animated:YES ];
}


- (void)addCreateNewEventView
{
    SmileyView* mView = [SmileyView initWithPoint:CGPointMake(0, -240)];
    mView.tag = 90000;
    mView.delegate = self;
    mView.shouldBeRemoved = NO;
    [mView showCancelButton:NO];
    [listSubViews addObject:mView];
    [listSubViews addObject:tblTimeline];
    
    [mScrollView addSubview:mView];
    
    
    [self initSubViewsLayout];
}

- (IBAction)btnShowSmiley:(id)sender
{
    int randNum = rand() % 100;
    SmileyView* mView = [SmileyView initWithPoint:CGPointMake(0, -240)];
    mView.tag = 90001+randNum;
    mView.delegate = self;
    mView.shouldBeRemoved = YES;
    [mView showCancelButton:YES];
    [listSubViews insertObject:mView atIndex:0];
    
    [mScrollView addSubview:mView];
    
    [self initSubViewsLayout];
}

- (void)checkShouldRemoveSmileyView
{
    for (int i = 0; i < listSubViews.count; i++) {
        UIView* mView = (UIView*)[listSubViews objectAtIndex:i];
        if ([mView isKindOfClass:[SmileyView class]] && !((SmileyView*)mView).isRemoved) {
            
            if ([((SmileyView*)mView) checkIsOutOfTime]) {
                ((SmileyView*)mView).isRemoved = YES;
                [((SmileyView*)mView) removeFromSuperview];
            }
        }
    }
    [self initSubViewsLayout];
}


#pragma - mark INIT LAYOUT

- (void)addPreviousSmileyView
{
    NSArray* mArr =  [Lib getSmileyViews];
    for (NSDictionary* mDic in mArr) {
        SmileyView* mView = [SmileyView initWithPoint:CGPointMake(0, -240)];
        mView.tag = [(NSNumber*)[mDic objectForKey:@"_tag"] integerValue];
        mView.delegate = self;
        mView.shouldBeRemoved = YES;
        [mView showCancelButton:YES];
        [mView setMeetingTitle:[mDic objectForKey:@"_title"] startAt:(NSDate*)[mDic objectForKey:@"_date"]];
        [listSubViews insertObject:mView atIndex:0];
        
        [mScrollView addSubview:mView];
        
    }
    [self checkShouldRemoveSmileyView];
}


- (CGFloat)estimateTableViewHeight
{
    arrMoodTimeline = [LCoreData getAllMoodTimeline];
    if (arrMoodTimeline == nil) {
        return 0;
    }
    CGFloat mResult = 0;
    for (MoodTimeline* m in arrMoodTimeline) {
        
        CGSize sizeText = [self sizeOfMultiLineLabel:m.note];
        mResult += sizeText.height+ 80;
    }
    
    return mResult;
}

- (void)initSubViewsLayout
{
   
    
    NSMutableArray* frameArr = [NSMutableArray array];
    CGFloat currentY = 50;
    
    
    for (UIView* mView in listSubViews) {
        if (![mView isKindOfClass:[SmileyView class]] || !((SmileyView*)mView).isRemoved) {
            CGRect mRect = mView.frame;
            [frameArr addObject:[NSNumber numberWithFloat:currentY]];
            currentY += mRect.size.height+10;
        }else{
            [frameArr addObject:[NSNumber numberWithInt:-300]];
        }
        
    }
    
    currentY -= tblTimeline.frame.size.height-10;
    for (int i = 0; i < listSubViews.count; i++) {
        UIView* mView = (UIView*)[listSubViews objectAtIndex:i];
        if (![mView isKindOfClass:[SmileyView class]] || !((SmileyView*)mView).isRemoved) {
            float mY = [[frameArr objectAtIndex:i] floatValue];
            CGRect mRect = mView.frame;
            mRect.origin.y = mY;
            [UIView animateWithDuration:.7 animations:^{
                mView.frame = mRect;
            }];
        }
        
        
    }
    
    
    CGRect mTableFrame = tblTimeline.frame;
    mTableFrame.size.height = [self estimateTableViewHeight];
    tblTimeline.frame = mTableFrame;
    
    mScrollView.contentSize = CGSizeMake(self.view.frame.size.width, currentY+mTableFrame.size.height+80);
    CGRect mTimeLineRect = viewTimeLine.frame;
    mTimeLineRect.size.height = mScrollView.contentSize.height > 800 ? mScrollView.contentSize.height:800;
    viewTimeLine.frame = mTimeLineRect;
}

- (void)changeTimer:(int)secondsChange withTag:(int)tag
{
    SmileyView *mView = (SmileyView*)[mScrollView viewWithTag:tag];
    if ([mView isKindOfClass:[SmileyView class]]) {
        [mView changeTimer:secondsChange];
    }
}


- (void)scheduleShowIncomingEventNotification
{
    if (_eventsList == nil || [_eventsList count] < 1 ||selectedCalendar == MPM_CLOUD_CALENDAR_NONE) {
        return;
    }
    
    NSDate* mNow = [NSDate date];
    for (MPMCalendarObject* mObj in _eventsList) {
        if ([mObj.MPM_StartDate compareToDate:mNow] >= 0) {
            [self scheduleShowIncomingEventNotificationAtDate:mObj.MPM_StartDate timeZone:mObj.MPM_TimeZone title:mObj.MPM_Title eventId:mObj.MPM_ID];
        }
    }
}

- (void)scheduleShowIncomingEventNotificationAtDate:(NSDate*)mDate timeZone:(NSTimeZone*)timeZone title:(NSString*)title eventId:(NSString*)mId
{
    
    if ([Lib checkExistNotification:mId]) {
        return;
    }
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [self estimateNotificationDate:mDate];
    int minute = ceil([mDate timeIntervalSinceDate:localNotification.fireDate] / 60);
    localNotification.alertBody = FORMAT([Lib localizedWithKey:@"MPM_KEY_NOTIFICATION"],title,minute);
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:MPM_NOTIFICATION_TYPE_COMING_EVENT,NOTIFICATION_TYPE,mDate, NOTIFICATION_DATE, title, NOTIFICATION_TITLE, mId, NOTIFICATION_ID, nil];
    [Lib addNotification:mId];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
}

#pragma - mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag >= 20000 ) {
        if (buttonIndex == 1) { // load event
            int randNum = [[NSDate date] timeIntervalSince1970];
            SmileyView* mView = [SmileyView initWithPoint:CGPointMake(0, -240)];
            mView.tag = 100000000+randNum;
            mView.delegate = self;
            
            NSDictionary *mNotificationDic = [notificationArray objectAtIndex:(alertView.tag - 20000)];
            if (mNotificationDic) {
                [mView setMeetingTitle:[mNotificationDic objectForKey:NOTIFICATION_TITLE] startAt:(NSDate*)[mNotificationDic objectForKey:NOTIFICATION_DATE]];
                [listSubViews insertObject:mView atIndex:0];
                
                
                [mScrollView addSubview:mView];
                
                [self initSubViewsLayout];
            }
            
        }
        [self gotoNextEvent];
    }
    else if (alertView.tag == 10010 && buttonIndex == 1) {
        [self gotoNextEvent];
    }
}

#pragma mark - Next event and cloud calendar
//
//- (BOOL)checkIsNeedToNextEvent
//{
//    if (_eventsList == nil || _eventsList.count < 1 || selectedCalendar == MPM_CLOUD_CALENDAR_NONE) {
//        return NO;
//    }
//    NSDate* mLastDate = [Lib getLastEventDate];
////    NSDate* mLastDate = [NSDate dateWithTimeInterval:-2*24*60*60 sinceDate:[NSDate date]];
//    if (mLastDate == nil) {
//        return NO;
//    }
//    [Lib logToFile:FORMAT(@"===== CHECK TO ADD  LASTDATE %@ ====", mLastDate)];
//    NSDate* mNow = [NSDate date];
//    for (int  i = currentIndex; i < _eventsList.count; i++) {
//        MPMCalendarObject* mObj = (MPMCalendarObject*)[_eventsList objectAtIndex:i];
//        [Lib logToFile:FORMAT(@"===== CHECK TO ADD %@ ====", mObj.MPM_ID)];
//        if ([mObj.MPM_StartDate compareToDate:mNow] <= 0  && [mObj.MPM_StartDate compareToDate:mLastDate] > 0) {
//            // Auto add this event to timeline
//            currentIndex  = i;
//            [[NSUserDefaults standardUserDefaults] setObject:mObj.MPM_StartDate forKey:selectedCalendar != MPM_CLOUD_CALENDAR_GOOGLE ? MPM_USER_LAST_EVENT_TIME_GOOGLE : MPM_USER_LAST_EVENT_TIME_ICLOUD];
//            
//            int randNum = [mObj.MPM_StartDate timeIntervalSince1970];
//            SmileyView* mView = [SmileyView initWithPoint:CGPointMake(0, -240)];
//            mView.tag = 100000000+randNum;
//            mView.delegate = self;
//            
//            [mView setMeetingTitle:mObj.MPM_Title startAt:mObj.MPM_StartDate];
//            [listSubViews insertObject:mView atIndex:0];
//            
//            
//            [mScrollView addSubview:mView];
//            [Lib logToFile:FORMAT(@"===== ADDED %@ ====", mObj.MPM_ID)];
//
//        }
//    }
//    
//    [self initSubViewsLayout];
//    
//    [self gotoNextEvent];
//    
//    return NO;
//}

@end
