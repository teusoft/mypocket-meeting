//
//  ForgotPasswordView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/4/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lib.h"
#import "AFNetworking.h"

@interface ForgotPasswordView : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

@property (strong, nonatomic) IBOutlet UILabel *lblError;


@end
