//
//  Uiimage+Picker.h
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/18/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Picker)

- (UIColor *)colorAtPixel:(CGPoint)point;

@end
