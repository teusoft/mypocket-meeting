//
//  QuickTipsView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/27/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuickTipsViewDelegate

@optional
- (void)quickTipsViewWillClose;

@end

@interface QuickTipsView : UIViewController <UIScrollViewDelegate> {
    
    __weak IBOutlet UITextView *txtTips1, *txtTips2, *txtTips3;
    __weak IBOutlet UIButton *btnLike1, *btnLike2, *btnLike3;
    
    __weak IBOutlet UIScrollView *scrollContent;
    __weak IBOutlet UIPageControl *pageControl;
    
    BOOL pageControlBeingUsed;
}

@property (nonatomic, strong) id <QuickTipsViewDelegate>delegate;
@property int moodType;

- (void)loadTipForMoodId:(int)moodId;

@end
