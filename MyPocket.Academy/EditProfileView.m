//
//  EditProfileView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "EditProfileView.h"
#import "MyProfileView.h"
#import "ObjEvent.h"

#import "BSKeyboardControls.h"

#import "Profile.h"
#import "LCoreData.h"

#import "AFNetworking.h"
#import "Lib.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ServiceLib.h"
#import "JSONKit.h"
#import "CountriesController.h"

@interface EditProfileView ()<CountriesControllerDelegate>

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end

@implementation EditProfileView {
    Profile *profileObject;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    profileObject = [LCoreData getProfile];
    
    [self loadProfileToForm];
    
    NSArray *arrInputControl = @[txtName, txtOccupation, txtPassword, txtConfirmPassword, txtCountry, txtCompany];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:arrInputControl]];
    [self.keyboardControls setDelegate:self];
    
    //set avatar
    imgAvatar.layer.cornerRadius =imgAvatar.frame.size.width / 2;
    imgAvatar.clipsToBounds = YES;
    imgAvatar.layer.borderWidth = 2.0f;
    imgAvatar.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [imgAvatar setImageWithURL:[NSURL URLWithString:profileObject.avatar]
              placeholderImage:[UIImage imageNamed:@"icon_avatarNoImage"]  usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    //set cover image
    
    [imgCover setImageWithURL:[NSURL URLWithString:profileObject.avatar]
             placeholderImage:[UIImage imageNamed:@"icon_avatarNoImage"]  usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    scrollContent.contentSize = CGSizeMake(scrollContent.frame.size.width, 720+10);
    btnUpdate.enabled = YES;
    
    birthDayView.hidden = YES;
    birthDayView.alpha = 0.0;
    birthDaySubView.layer.cornerRadius = 4;
    birthDaySubView.layer.masksToBounds = YES;
    birthDaySubView.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideStep2:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    if (imgAvatar.image == nil) {
        imgAvatar.image = [UIImage imageNamed:@"icon_avatarNoImage"];
    } else {
        [btnChangeAvatar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:[segCloudCalendar selectedSegmentIndex] ] forKey:MPM_USER_CLOUD_CALENDAR_MODE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadProfileToForm {
    txtName.text = profileObject.name;
    txtOccupation.text = profileObject.occupation;
    txtEmail.text = profileObject.email;
    txtPassword.placeholder = txtConfirmPassword.placeholder = [Lib localizedWithKey:@"MPM_KEY_OPTIONAL"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"M/d/yyyy"];
    _birthday = profileObject.birthday == nil ? @"" : [dateFormatter stringFromDate:profileObject.birthday];
    [btnBirthDay setTitle:_birthday forState:UIControlStateNormal];
    txtCountry.text = profileObject.country;
    txtCompany.text = profileObject.company;
    
    NSString *gender = [NSString stringWithFormat:@"%@",profileObject.gender];
    if ([gender isEqualToString:@"1"]) {
        segGenger.selectedSegmentIndex = 0;
    } else if ([gender isEqualToString:@"0"]) {
        segGenger.selectedSegmentIndex = 1;
    } else {
        segGenger.selected = NO;
    }
    
    imgLogo.image = [profileObject.isFacebook boolValue] ?[UIImage imageNamed:@"icon_avatarFacebook"]:[UIImage imageNamed:@"icon_avatarMyPocket"];
    imgPasswordBackground.image = imgLogoConfirmPasswordBackground.image =  [profileObject.isFacebook boolValue] ?[UIImage imageNamed:@"bg_textInputEditProfile_disable"]:[UIImage imageNamed:@"bg_textInputEditProfile"];
    txtPassword.enabled = txtConfirmPassword.enabled = btnChangeAvatar.enabled = ![profileObject.isFacebook boolValue] ;
    [btnChangeAvatar setTitle:[profileObject.isFacebook boolValue] ? @"Facebook":[Lib localizedWithKey:@"MPM_KEY_CHANGE_AVATAR"] forState:UIControlStateNormal];
    mSelectedCloudCalendar = [Lib getCloudCalendarMode];
    segCloudCalendar.selectedSegmentIndex = mSelectedCloudCalendar;
}

- (IBAction)btnCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnLogout:(id)sender {
    
    [LCoreData clearProfile];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}



// update user infor
- (void)serviceUpdateUserInfor
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{
                             @"token": profileObject.userToken,
                             @"name":txtName.text,
                             @"pwd":txtPassword.text,
                             @"occupation":txtOccupation.text,
                             @"gender":segGenger.selectedSegmentIndex?@"false":@"true",
                             @"birthday":_birthday,
                             @"country":txtCountry.text,
                             @"company":txtCompany.text
                             };
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, UPDATE_PROFILE_SERVICE];
    
    NSString* serviceTitle = @"UPDATE USER INFO";
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    NSOperation* op = [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                
                profileObject.name = txtName.text;
                profileObject.occupation = txtOccupation.text;
                profileObject.password = txtPassword.text;
                profileObject.gender = [NSNumber numberWithInteger:segGenger.selectedSegmentIndex];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"M/d/yyyy"];
                profileObject.birthday = [dateFormatter dateFromString:btnBirthDay.titleLabel.text];
                profileObject.country = txtCountry.text;
                profileObject.company = txtCompany.text;
                [LCoreData saveContext];
                
                
                if ([[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_NEEDED_TO_UPDATE_INFO]) {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_NEEDED_TO_UPDATE_INFO] boolValue]) {
                        //[self.navigationController popToRootViewControllerAnimated:NO];
                        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"menuView"];
                        
                        [self.navigationController pushViewController:controller animated:YES];
                        
                    }
                }
                
                [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_SUCCESS_YOUR_PROFILE_IS_UPDATED"]];
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                lblError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            lblError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        lblError.text = [error localizedDescription];
        [Lib showAlertMessage: [error localizedDescription]];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}



- (IBAction)btnUpdate:(id)sender {
   
    if (!btnUpdate.enabled) {
        return;
    }
    if ([self checkValidForm]){
        [self serviceUpdateUserInfor];
    }
}


- (IBAction)btnDeleteAcc:(id)sender {
    if ([Lib checkInternetConnectionWithMessage]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:MPM_ALERT_TITLE message:[Lib localizedWithKey:@"MPM_KEY_CONFIRM_DELETE_ACCOUNT"] delegate:self cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_CANCEL"] otherButtonTitles:[Lib localizedWithKey:@"MPM_KEY_OK"],nil];
        alertView.tag = 100;
        [alertView show];
    }
}


// delete user infor
- (void)serviceDeleteAccount
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"token": profileObject.userToken};
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, DELETE_ACCOUNT_SERVICE];
    
    NSString* serviceTitle = @"DELETE ACCOUNT";
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    NSOperation *op = [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                
                [LCoreData clearProfile];
                
                [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_SUCCESS_ACCOUNT_IS_DELETED"]];
                
                [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                lblError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
                [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            lblError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
        }
        [Lib removeIndcatorViewOn:self.view];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        lblError.text = [error localizedDescription];
        [Lib showAlertMessage:[error localizedDescription]];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}


- (IBAction)btnChangeAvatar:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [btnChangeAvatar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [self doUpdateAvatar:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)doUpdateAvatar:(UIImage*)savedImage{
    [Lib showIndicatorViewOn2:self.view];
    
    // do Update avatar to server
    if (savedImage != nil)
    {
        NSString *serviceUrl = [NSString stringWithFormat:@"%@%@", SERVICE_URL, UPDATE_AVATAR_SERVICE];
        
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:serviceUrl]];
        
        NSData *imageData = UIImageJPEGRepresentation(savedImage, 0.7);
        
        NSDictionary *parameters = @{@"token": profileObject.userToken};
        
        NSString* serviceTitle = @"UPDATE AVATAR";
        
        [Lib logRequestWithName:serviceTitle url:serviceUrl params:parameters];
        
        AFHTTPRequestOperation *op = [manager POST:serviceUrl parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //do not put image inside parameters dictionary as I did, but append it!
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
            NSDictionary* results = (NSDictionary *)responseObject;;
            if (results) {
                
                if ([[responseObject objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue]) {
                    imgAvatar.layer.cornerRadius =imgAvatar.frame.size.width / 2;
                    imgAvatar.clipsToBounds = YES;
                    imgAvatar.layer.borderWidth = 2.0f;
                    imgAvatar.layer.borderColor = [UIColor whiteColor].CGColor;
                    imgAvatar.image = savedImage;
                    //set cover image
                    imgCover.image = savedImage;
                    
                    profileObject.avatar = [responseObject objectForKey:MPM_SERVICE_RESPONSE_DATA ];
                    [LCoreData saveContext];
                    [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_SUCCESS_YOUR_PROFILE_IS_UPDATED"]];
                    
                    [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                    
                    
                }else{
                    [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                    lblError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
                    [Lib showAlertMessage:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                }
            }else{
                [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
                lblError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
                [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            }
            [Lib removeIndcatorViewOn:self.view];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [Lib removeIndcatorViewOn:self.view];
            [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
            lblError.text = [error localizedDescription];
            [Lib showAlertMessage: [error localizedDescription]];
            
        }];
        if (op == nil) {
            [Lib removeIndcatorViewOn:self.view];
        }else{
            [op start];
        }
    }

}

#pragma mark - keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self btnUpdate:nil];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    lblError.text = @"";
    btnUpdate.enabled = YES;
    [self.keyboardControls setActiveField:textField];
    
    CGPoint tempPoint = textField.superview.center;
    tempPoint.y = tempPoint.y - 50;
    tempPoint.x = 0;
    
    [scrollContent setContentOffset:tempPoint animated:YES];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)keyboardWillHideStep2: (NSNotification*) notif{
    scrollContent.contentSize = CGSizeMake(scrollContent.frame.size.width, 720+10);

    [scrollContent setContentOffset:CGPointZero animated:YES];
}

- (void)keyboardWillShow:(NSNotification*)notif{
    scrollContent.contentSize = CGSizeMake(scrollContent.frame.size.width, 720+200);
}



- (void)segCloudCalendarChanged:(id)sender
{
    if (_googleOAuth == nil) {
        // Initialize the googleOAuth object.
        // Pay attention so as to initialize it with the initWithFrame: method, not just init.
        _googleOAuth = [[GoogleOAuth alloc] initWithFrame:self.view.frame];
        // Set self as the delegate.
        [_googleOAuth setGOAuthDelegate:self];
        mSelectedCloudCalendar = MPM_CLOUD_CALENDAR_NONE;
    }

    if (((UISegmentedControl*)sender).selectedSegmentIndex == MPM_CLOUD_CALENDAR_NONE) {
        [_googleOAuth revokeAccessToken];
    }else if (((UISegmentedControl*)sender).selectedSegmentIndex == MPM_CLOUD_CALENDAR_GOOGLE) {
        if (![Lib checkInternetConnectionWithMessage]) {
            segCloudCalendar.selectedSegmentIndex = mSelectedCloudCalendar;
            return;
        }
        _arrGoogleCalendars = nil;
        
        
        UIViewController* googleOAuthController = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleOAuthViewController"];
        UIButton* btnCancel = (UIButton*)[googleOAuthController.view viewWithTag:100];
        [btnCancel addTarget:self action:@selector(btnCancelGoogleOAuthClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [_googleOAuth authorizeUserWithClienID:GOOGLE_CLIENT_ID
                               andClientSecret:GOOGLE_CLIENT_SECRET
                                 andParentView:[googleOAuthController.view viewWithTag:10]
                                     andScopes:[NSArray arrayWithObject:@"https://www.googleapis.com/auth/calendar"]];
        
        [self presentViewController:googleOAuthController animated:YES completion:nil];
        

    }else if (((UISegmentedControl*)sender).selectedSegmentIndex == MPM_CLOUD_CALENDAR_ICLOUD) {
        [_googleOAuth revokeAccessToken];
        [self checkEventStoreAccessForCalendar];
    }
    
}



// Check the authorization status of our application for Calendar
-(void)checkEventStoreAccessForCalendar
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    if (status == EKAuthorizationStatusAuthorized) {
        mSelectedCloudCalendar = MPM_CLOUD_CALENDAR_ICLOUD;
    }
    else if(status == EKAuthorizationStatusNotDetermined)
    {
        [[[EKEventStore alloc] init] requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
         {
             if (error || !granted)
             {
                 
                 segCloudCalendar.selectedSegmentIndex = mSelectedCloudCalendar;
                 [Lib logToFile:[error localizedDescription]];
                 [Lib showAlertMessage:FORMAT([Lib localizedWithKey:@"MPM_WARNING_ERROR_CALENDAR"],[error localizedDescription])];
             }else{
                 
                 mSelectedCloudCalendar = MPM_CLOUD_CALENDAR_ICLOUD;
             }
         }];
    }else if(status == EKAuthorizationStatusDenied || status == EKAuthorizationStatusRestricted)
    {
        segCloudCalendar.selectedSegmentIndex = mSelectedCloudCalendar;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Lib localizedWithKey:@"MPM_WARNING_PRIVACY"] message:[Lib localizedWithKey:@"MPM_WARNING_DETAIL"]
                                                       delegate:nil
                                              cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_OK"]
                                              otherButtonTitles:nil];
        [alert show];
    }
}


- (void)btnCancelGoogleOAuthClicked:(id)sender
{
    segCloudCalendar.selectedSegmentIndex = mSelectedCloudCalendar;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GoogleOAuth class delegate method implementation


-(void)authorizationWasSuccessful{
    
    [Lib removeIndcatorViewOn:self.view];
    mSelectedCloudCalendar = MPM_CLOUD_CALENDAR_GOOGLE;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)responseFromServiceWasReceived:(NSString *)responseJSONAsString andResponseJSONAsData:(NSData *)responseJSONAsData
{
    
}

-(void)accessTokenWasRevoked{
    // Remove all calendars from the array.
    [_arrGoogleCalendars removeAllObjects];
    _arrGoogleCalendars = nil;
    
    [Lib removeIndcatorViewOn:self.view];
}


-(void)errorOccuredWithShortDescription:(NSString *)errorShortDescription andErrorDetails:(NSString *)errorDetails{
    // Just log the error messages.
    NSLog(@"%@", errorShortDescription);
    NSLog(@"%@", errorDetails);
    
    [Lib removeIndcatorViewOn:self.view];
}


-(void)errorInResponseWithBody:(NSString *)errorMessage{
    // Just log the error message.
    NSLog(@"%@", errorMessage);
    
    [Lib removeIndcatorViewOn:self.view];
}

- (BOOL)checkValidForm
{
    NSString* errString = @"";
    BOOL isError = NO;
    
    
    if (([txtPassword.text length] > 0 || [txtConfirmPassword.text length] > 0) && ![txtPassword.text isEqualToString:txtConfirmPassword.text]) {
        errString = [errString appendAtNewLine:[Lib localizedWithKey:@"MPM_KEY_ERROR_PASSWORD_NOT_SAME_CONFIRM_PASSWORD"]];
        isError = YES;
    }
    
    if ([profileObject.isFacebook boolValue]) {
        if ([txtName.text isNullOrEmpty])
        {
            errString = [errString appendAtNewLine:[Lib localizedWithKey:@"MPM_KEY_ERROR_NAME_MUST_NOT_EMPTY"]];
            isError = YES;
        }
    }else{
        if ([txtName.text isNullOrEmpty] ||[txtOccupation.text isNullOrEmpty]||[btnBirthDay.titleLabel.text isNullOrEmpty]||[txtCountry.text isNullOrEmpty]||[txtCompany.text isNullOrEmpty]
            ) {
            errString = [errString appendAtNewLine:[Lib localizedWithKey:@"MPM_KEY_ERROR_ALL_MUST_NOT_BE_EMPTY"]];
            isError = YES;
        }
    }
    
    
    if (isError) {
        [Lib showAlertMessage:errString];
    }
    return !isError;
}


- (void)chooseCountry:(id)sender
{
    CountriesController *countriesViewController = (CountriesController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CountriesViewController"];
    countriesViewController.delegate = self;
    countriesViewController.curCountryCode = [txtCountry text];
    [self.navigationController pushViewController:countriesViewController animated:YES];
}

- (void)selectCountry:(NSString *)countryCode
{
    txtCountry.text = countryCode;
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 && buttonIndex == 1) {
        [self serviceDeleteAccount];
    }
}



#pragma mark BirthDay View

- (IBAction)btnBirthDayClicked:(id)sender
{
    if ( profileObject.birthday == nil) {
        mDatePicker.date = [NSDate date];
    }else{
        mDatePicker.date = profileObject.birthday;
    }
    mDatePicker.maximumDate = [NSDate date];
    [self showAnimateBirthDay];
}

- (void)showAnimateBirthDay
{
    birthDayView.transform = CGAffineTransformMakeScale(1.3, 1.3);
    birthDayView.alpha = 0;
    birthDayView.hidden = NO;
    [UIView animateWithDuration:.25 animations:^{
        birthDayView.alpha = 1;
        birthDayView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        if (finished) {
            birthDayView.userInteractionEnabled = YES;
        }
    }];
}

- (void)removeAnimateBirthDay
{
    [UIView animateWithDuration:.25 animations:^{
        birthDayView.transform = CGAffineTransformMakeScale(1.3, 1.3);
        birthDayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            birthDayView.hidden = YES;
        }
    }];
}



- (IBAction)dismissBirthDayView:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"M/d/yyyy"];
    [btnBirthDay setTitle:[dateFormatter stringFromDate:mDatePicker.date] forState:UIControlStateNormal];
    _birthday = [dateFormatter stringFromDate:mDatePicker.date];
    
    [self removeAnimateBirthDay];
}


- (IBAction)dismissBirthDayViewClear:(id)sender
{
    _birthday = @"";
    [btnBirthDay setTitle:@"" forState:UIControlStateNormal];
    
    [self removeAnimateBirthDay];
}

@end
