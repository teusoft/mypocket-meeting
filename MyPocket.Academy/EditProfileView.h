//
//  EditProfileView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleOAuth.h"
#import "BSKeyboardControls.h"
#import "Lib.h"
#import "AFNetworking.h"

@interface EditProfileView : UIViewController <BSKeyboardControlsDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,GoogleOAuthDelegate, UIAlertViewDelegate> {

    __weak IBOutlet UIScrollView *scrollContent;
    __weak IBOutlet UIButton *btnChangeAvatar;
    __weak IBOutlet UITextField *txtName;
    __weak IBOutlet UITextField *txtOccupation;
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet UITextField *txtConfirmPassword;
    __weak IBOutlet UIButton *btnBirthDay;
    __weak IBOutlet UITextField *txtCountry;
    __weak IBOutlet UITextField *txtCompany;
    __weak IBOutlet UISegmentedControl *segGenger;
    __weak IBOutlet UISegmentedControl *segCloudCalendar;
    
    __weak IBOutlet UILabel *lblError;
    
    __weak IBOutlet UIButton *btnUpdate;
    __weak IBOutlet UIButton *btnDeleteAcc;
    
    __weak IBOutlet UIImageView *imgAvatar;
    __weak IBOutlet UIImageView *imgCover;
    
    
    __weak IBOutlet UIView *birthDayView;
    __weak IBOutlet UIView *birthDaySubView;
    
    __weak IBOutlet UIImageView *imgLogo;
    __weak IBOutlet UIImageView *imgPasswordBackground;
    __weak IBOutlet UIImageView *imgLogoConfirmPasswordBackground;
    
    NSInteger mSelectedCloudCalendar;
    __weak IBOutlet UIDatePicker* mDatePicker;
    NSString* _birthday;
    
}
@property (nonatomic, strong) GoogleOAuth *googleOAuth;

@property (nonatomic, strong) NSMutableArray *arrGoogleCalendars;
@property (nonatomic, strong) NSMutableArray *eventsList;
- (IBAction)segCloudCalendarChanged:(id)sender;


- (IBAction)chooseCountry:(id)sender;

@end