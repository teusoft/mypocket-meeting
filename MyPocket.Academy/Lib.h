//
//  Lib.h
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/31/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define IS_IPHONE_5 ( IS_IPHONE && IS_HEIGHT_GTE_568 )

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>


@interface UILabel(ExtendedMethods)
-(void)willDismissAfterSeconds:(int)seconds;
- (void)dissmissNow;
@end

@interface NSDate (ExtendedMethods)
- (int)compareToDate:(NSDate*)mDate;
@end


@interface NSString (Utilities)
- (BOOL) isEmail;
- (NSString*)trim;
- (NSString*)stringWithWithCapitalizeFirstLetter;
- (BOOL)isNullOrEmpty;
- (NSString*)append:(NSString*)str;
- (NSString*)appendAtNewLine:(NSString*)str;
@end

@interface NSMutableArray (ExtendedMethods)
- (void)shuffle;
- (void)reverse;
@end

@interface UIImage (Crop)
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
- (UIImage *)crop:(CGRect)rect;
@end

@interface UIView(Extended)
- (UIImage *) imageByRenderingView;
@end

@interface NSDictionary(Extended)
- (id)getObjectForKey:(id)aKey;
@end

@interface NSMutableDictionary(Extended)
- (void)setTheObject:(id)anObject forKey:(id <NSCopying>)aKey;
@end

@interface UIColor (HexColors)

+(UIColor *)colorWithHexString:(NSString *)hexString;
+(NSString *)hexValuesFromUIColor:(UIColor *)color;

@end

@interface Lib : NSObject {
    
}

+ (BOOL)isIpad;
+(NSURL*)URLFromString:(NSString*)urlStr;
+ (NSString *) isNull:(id)object;
+ (NSArray*)sortListData:(NSArray*)dataSource sortWithKey:(NSString*)myKey isAsc:(BOOL)isAsc;
+(BOOL)object:(id)object classNamed:(NSString*)name;

+ (BOOL)isIphone;
+ (BOOL)isPhone5;

+(NSString*)docDirPath;
+ (void)showAlertMessage:(NSString*)msg;
+(void)showAlert:(NSString*)title withMessage:(NSString*)msg;
+(id)getValueOfKey:(NSString*)key;
+(void)setValue:(id)value ofKey:(NSString*)key;

+(void)showLoadingViewOn2:(UIView *)aView withAlert:(NSString *)text;
+(void)removeLoadingViewOn:(UIView *)superView;
+(NSDate*)getThisWeek;
+(NSDate*)getThisMonth;
+(NSDate*)getLastMonth;
+(void)customWebview : (UIWebView *)aWebView;
+(void)showIndicatorViewOn2:(UIView *)aView;
+(void)removeIndcatorViewOn:(UIView *)superView;

+(void)saveString:(NSString*)_str forKey:(NSString*)_key;
+(NSString*)getStringForKey:(NSString*)_key;

//+(NSString*)getCountryNameISO3166:(NSString*)countryCode;

+(void)saveImage:(UIImage*)image withPath:(NSString*)imagePath;

+ (BOOL) checkRetina;

+(UIImage*)scaleAndRotateImage:(UIImage *)image;
+ (UIColor *)randomColor;
+ (void)showAlertNoInternetConnection;
+(void)showConfirmAlert:(NSString*)title withMessage:(NSString*)msg delegate:(UIViewController*)delegate;

+ (NSString *)getFahrenheitFromCelsius:(NSString*)temp;
+ (NSString *)getKelvinFromCelsius:(NSString*)temp;

+(BOOL)checkInternetConnection;
+(BOOL)checkInternetConnectionWithMessage;
+(BOOL)checkInternetConnectionWithMessage:(NSString*)warningMessage;
+ (NSString *)timeFromSeconds:(double)totalSeconds showSeconds:(BOOL)show;
+ (BOOL)internetConnected;
+ (NSString *)filterStr:(NSString *)str withCharacters:(NSString *)charSet;


+ (NSDate*)timestampToDate:(NSString*)timeStampString;

+(void)logToFile:(NSString*)message;
+(void)logDictionary:(NSDictionary*)dic serviceName:(NSString*)serviceName;
+(void)logArray:(NSArray*)dic serviceName:(NSString*)serviceName;
+ (void)logRequestWithName:(NSString*)serviceName url:(NSString*)url params:(NSDictionary*)params;
+ (void)logResponseWithName:(NSString*)serviceName response:(NSString*)msg;
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;


+ (NSString*)localizedWithKey:(NSString*)key;


+ (void)resetUserData;
+ (BOOL)checkExistNotification:(NSString*)mId;
+ (void)addNotification:(NSString*)mId;
+ (void)removeNotification:(NSString*)mId;


+ (void)setLastEventDate;
+ (NSDate*)getLastEventDate;
+ (void)setLastEventDate:(NSDate*)mDate;

+ (NSArray*)getSmileyViews;

+ (void)cleanSmileyViews;
+ (void)setBadgeNumber;

+ (void)addSmileyViewWithTitle:(NSString*)title startDate:(NSDate*)mDate tag:(NSNumber*)mTag;
+ (BOOL)isValidAge:(NSString*)age;

+ (int)getCloudCalendarMode;

+ (NSString *) appVersionNumber;
+ (NSString *) appBuildNumber;
+ (NSString*) appBuildVersionNumber;
@end
