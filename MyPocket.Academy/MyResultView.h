//
//  MyResultView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/26/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLPieChart.h"

#import "Profile.h"
#import <sqlite3.h>

@interface MyResultView : UIViewController <UITabBarDelegate>{
    __weak IBOutlet UISegmentedControl *segControl;
    __weak IBOutlet UILabel *lbNoData;

    NSArray *arrResult;
    sqlite3 *db;

    Profile *profileObject;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblOccupation;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblLastLogin;

@property (nonatomic, retain) IBOutlet DLPieChart *chartBefore, *chartAfter;

@property (strong, nonatomic) IBOutlet UITabBarItem *tabBarItemResult;


@end
