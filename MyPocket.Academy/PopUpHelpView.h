//
//  PopUpHelpView.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@protocol PopUpHelpViewDelegate

- (void)popUpHelpViewDidSelect;

@end

@interface PopUpHelpView : UIViewController

@property(nonatomic, strong)id<PopUpHelpViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIView *popUpView;

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated;

@end
