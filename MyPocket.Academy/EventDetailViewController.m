//
//  EventDetailViewController.m
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/24/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "EventDetailViewController.h"
#import "Lib.h"

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:MPM_TIME_DATE_FORMAT];

    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.7];
    self.addCommentView.layer.cornerRadius = 0;
    
    _txtTextComment.text = self.currentEvent.MPM_Title;
    _lblStartTime.text = [dateFormat stringFromDate:self.currentEvent.MPM_StartDate];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (void)closePopup {
    [self removeAnimate];
}

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}


- (void)showPopUpWithTitle:(NSString *)title atDate:(NSDate *)mDate inView:(UIView *)aView animated:(BOOL)animated
{
    self.currentEvent = [MPMCalendarObject new];
    self.currentEvent.MPM_StartDate = mDate;
    self.currentEvent.MPM_Title = title;
    
    [self showPopUp:aView animated:animated];
    self.btnRate.enabled = self.btnSkip.enabled = NO;
    self.viewTitle.text = [Lib localizedWithKey:@"MPM_KEY_EVENT_DETAIL"];
    [self.btnRate setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.btnSkip setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
}


- (IBAction)btnOk:(id)sender {
    [self removeAnimate];
}

- (IBAction)btnCancel:(id)sender {
    [self removeAnimate];
    
    [self.delegate skipEvent];
}

- (void)btnRateNowClicked:(id)sender
{
    [self removeAnimate];
    [self.delegate eventDetailDidTapToRateButtonForEvent:self.currentEvent];
}

@end
