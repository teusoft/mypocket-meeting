//
//  MoodCell.h
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 9/6/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoodTimeline.h"

@interface MoodCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView             *bgView;
@property (weak, nonatomic) IBOutlet UIImageView        *img_bg;

@property (weak, nonatomic) IBOutlet UIView        *imgBefore;
@property (weak, nonatomic) IBOutlet UIView        *imgAfter;
@property (weak, nonatomic) IBOutlet UILabel            *lbTime;
@property (weak, nonatomic) IBOutlet UIButton            *btnComment;
@property (weak, nonatomic) IBOutlet UILabel            *lbComment;
@property (weak, nonatomic) IBOutlet UILabel            *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel            *lbLocalWarning;
@property (nonatomic, assign) BOOL didSetupConstraints;

-(void)setDetailWithData:(MoodTimeline *) moodItem;

@end
