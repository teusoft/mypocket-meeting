//
//  UIView+ColorOfPoint.h
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/14/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ColorOfPoint)
- (UIColor *) colorOfPoint:(CGPoint)point;
@end