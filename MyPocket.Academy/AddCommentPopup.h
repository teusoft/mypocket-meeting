//
//  AddCommentPopup.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/5/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoodTimeline.h"

@protocol AddCommentPopupDelegate

- (void)addCommentPopupDidUpdateComment;

@end

@interface AddCommentPopup : UIViewController{
    __weak IBOutlet UIView *addCommentView;
    __weak IBOutlet UITextView *txtTextComment;
}

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated;

@property(nonatomic, strong)id<AddCommentPopupDelegate>delegate;

@property (nonatomic, strong) MoodTimeline *timelineItem;

@end
