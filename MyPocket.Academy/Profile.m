//
//  Profile.m
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 11/7/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "Profile.h"


@implementation Profile

@dynamic userId;
@dynamic name;
@dynamic email;
@dynamic avatar;
@dynamic gender;
@dynamic birthday;
@dynamic country;
@dynamic company;
@dynamic occupation;
@dynamic lastLogin;
@dynamic isFacebook;
@dynamic isFirstLogin;
@dynamic password;
@dynamic meetingTimer;
@dynamic userToken;
@end
