//
//  ObjEvent.m
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/31/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "ObjEvent.h"
#import "Lib.h"

@implementation ObjEvent

-(id)initWithDic:(NSDictionary*)dic{
    
    self = [super init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    self.title    = [Lib isNull:[dic objectForKey:@"summary"]];
    self.startDate    = [dateFormat dateFromString:[[dic objectForKey:@"start"] objectForKey:@"dateTime"]];
    if (self.startDate) {
       
    }else{
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        self.startDate    = [dateFormat dateFromString:[[dic objectForKey:@"start"] objectForKey:@"date"]];
        self.startDate = [self.startDate dateByAddingTimeInterval:8*60*60];
    }
    
    if ([Lib isNull:[dic objectForKey:@"end"]].length) {
        self.endDate    = [dateFormat dateFromString:[[dic objectForKey:@"end"] objectForKey:@"dateTime"]];
    }
    self.status    = [Lib isNull:[dic objectForKey:@"status"]];

    if ([Lib isNull:[dic objectForKey:@"recurrence"]].length) {
        
        NSString *strRecurrence;
        strRecurrence = [[Lib isNull:[dic objectForKey:@"recurrence"]] stringByReplacingOccurrencesOfString:@"RRULE:" withString:@""];
        
        NSLog(@"recurrence: %@", strRecurrence);
        
        self.recurrenceRule = strRecurrence;
    }
    
    return self;
}

@end
