//
//  LoginViewController.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/22/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MyProfileView.h"
//#import "MenuTabBar.h"
#import "Lib.h"
#import "AFNetworking.h"
#import <FacebookSDK/FacebookSDK.h>
#import "BSKeyboardControls.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate, BSKeyboardControlsDelegate>{
    __weak IBOutlet UIView *loginView;
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet UILabel *txtError;
}

@end
