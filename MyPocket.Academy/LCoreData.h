//
//  LCoreData.h
//  ExpensesEntry
//
//  Created by Luthor on 10/8/13.
//  Copyright (c) 2013 Lex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Profile.h"
#import "MoodTimeline.h"

@interface LCoreData : NSObject

/*
    Profile
 */
+ (BOOL)addProfile:(NSDictionary*)itemData;
+ (BOOL)clearProfile;
+ (Profile *)getProfile;

/*
    MoodTimeline
 */
+ (MoodTimeline*)creatNewMoodTimeline;
+ (BOOL)addMoodTimeline:(NSDictionary*)itemData;
+ (BOOL)clearOnlineMoodTimeline;
+ (BOOL)clearAllMoodTimeline;
+ (NSArray *)getAllMoodTimeline;
+ (NSArray *)getLocalMoodTimeline;
+ (BOOL)deleteMoodTimeline:(MoodTimeline*)moodTimelineItem;

+ (BOOL)saveContext;

@end
