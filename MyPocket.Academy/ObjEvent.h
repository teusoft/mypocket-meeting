//
//  ObjEvent.h
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/31/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EKRecurrenceRule+RRULE.h"

@interface ObjEvent : NSObject

@property (nonatomic, retain) NSString  * title;
@property (nonatomic, retain) NSDate    * startDate;
@property (nonatomic, retain) NSDate    * endDate;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString *recurrenceRule;
@property (nonatomic, retain) NSString *eventIdentity;

-(id)initWithDic:(NSDictionary*)dic;

@end
