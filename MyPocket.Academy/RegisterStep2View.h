//
//  RegisterStep2View.h
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/29/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking/AFNetworking.h"
#import "Lib.h"
#import "BSKeyboardControls.h"
#import "CountriesController.h"

@interface RegisterStep2View : UIViewController <UITextFieldDelegate, BSKeyboardControlsDelegate, CountriesControllerDelegate>{
    
    __weak IBOutlet UIScrollView *scrollContent;
    
    __weak IBOutlet UITextField *txtFullName;
    
    __weak IBOutlet UISegmentedControl *gender;
    
    
    __weak IBOutlet UITextField *txtCountry;
    
    __weak IBOutlet UITextField *txtCompany;
    __weak IBOutlet UITextField *txtOccupation;
    
    __weak IBOutlet UITextField *txtError;
    
    __weak IBOutlet UIButton *btnBack;
    
    __weak IBOutlet UIButton *btnFinish;
    
    __weak IBOutlet UIButton *btnBirthday;
    __weak IBOutlet UIView *birthDayView;
    __weak IBOutlet UIView *birthDaySubView;
    
    __weak IBOutlet UIDatePicker* mDatePicker;
}

@property(nonatomic, strong) NSString *strEmail;
@property(nonatomic, strong) NSString *strPassword;

@end
