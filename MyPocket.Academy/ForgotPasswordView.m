//
//  ForgotPasswordView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/4/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "ForgotPasswordView.h"

@interface ForgotPasswordView ()

@end

@implementation ForgotPasswordView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnGo:(id)sender {
    _lblError.text = @"";
    if ([_txtEmail.text isEmail]) {
        [self serviceForgotPassword];
        
    } else {
        self.lblError.text = [Lib localizedWithKey:@"MPM_KEY_ERROR_EMAIL_IS_INVALID"];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    _lblError.text = @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    [self btnGo:nil];
    
    return YES;
}

// check email for registration
- (void)serviceForgotPassword
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"email": [_txtEmail.text lowercaseString]};
    
    NSString   *strUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL, FORGOT_PASSWORD_SERVICE];
    
    NSString* serviceTitle = @"FORGOT PASSWORD";
    
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    
    NSOperation *op= [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary* results = (NSDictionary *)responseObject;;
        if (results) {
            
            
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            
            if (isSuccess) {
                [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_SUCCESS_UPDATE_PASSWORD"]];
                [Lib logResponseWithName:serviceTitle response:MPM_SERVICE_RESPONSE_MESSAGE];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                _lblError.text = [results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE];
            }
            
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            _lblError.text = MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT;
        }
        [Lib removeIndcatorViewOn:self.view];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response:[error localizedDescription]];
        _lblError.text = [error localizedDescription];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        transition.subtype = kCATransitionFromLeft; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"firstLoginView"];
        
        [self presentViewController:controller animated:NO completion:nil];
    }
}


@end
