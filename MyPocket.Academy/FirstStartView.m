//
//  FirstStartView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/22/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "FirstStartView.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "Profile.h"

#import "LCoreData.h"

@interface FirstStartView ()

@end

@implementation FirstStartView {
    Profile *profileObject;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        profileObject = [LCoreData getProfile];
    
    if (profileObject) {
        NSNumber* mNum = [[NSUserDefaults standardUserDefaults] objectForKey:MPM_USER_NEEDED_TO_UPDATE_INFO];
        BOOL isNeededToUpdateData = [mNum boolValue];
        
        if (isNeededToUpdateData) [Lib showAlertMessage:[Lib localizedWithKey:@"MPM_KEY_WARNING_NEED_TO_UPDATE_PROFILE"]];
        if (isNeededToUpdateData) {
            
            UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"editProfileView"];
            [self.navigationController pushViewController:controller animated:YES];
            return;
        }
        

        [self switchView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)checkLogin {
//    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
//        NSLog(@"Found a cached session");
//        // If there's one, just open the session silently, without showing the user the login UI
//        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
//                                           allowLoginUI:NO
//                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//                                          // Handler for session state changes
//                                          // This method will be called EACH time the session state changes,
//                                          // also for intermediate states and NOT just when the session open
//                                          
//                                      }];
//        
//        
//        // If there's no cached session, we will show a login button
//    } //else {
//    //        UIButton *loginButton = [self.customLoginViewController loginButton];
//    //        [loginButton setTitle:@"Log in with Facebook" forState:UIControlStateNormal];
//    //    }
//}

- (void)switchView {
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"menuView"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

//- (void)checkLoginFacebook {
//    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
//        NSLog(@"Found a cached session");
//        // If there's one, just open the session silently, without showing the user the login UI
//        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
//                                           allowLoginUI:NO
//                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//                                          // Handler for session state changes
//                                          // This method will be called EACH time the session state changes,
//                                          // also for intermediate states and NOT just when the session open
//                                          [self switchView];
//                                      }];
//        
//        
//        // If there's no cached session, we will show a login button
//    } //else {
//    //        UIButton *loginButton = [self.customLoginViewController loginButton];
//    //        [loginButton setTitle:@"Log in with Facebook" forState:UIControlStateNormal];
//    //    }
//}

@end
