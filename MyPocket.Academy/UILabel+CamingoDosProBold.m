//
//  UILabel+CamingoDosProBold.m
//  MyPocket Meeting
//
//  Created by Nguyen Sy Tan on 12/17/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "UILabel+CamingoDosProBold.h"

@implementation UILabel_CamingoDosProBold

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = MPM_FONT_PRO_BOLD(self.font.pointSize);
}

@end
