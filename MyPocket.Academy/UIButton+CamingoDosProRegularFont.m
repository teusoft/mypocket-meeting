//
//  UIButton+CamingoDosProRegularFont.m
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/11/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "UIButton+CamingoDosProRegularFont.h"

@implementation UIButton_CamingoDosProRegularFont

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.titleLabel.font = MPM_FONT_PRO_REGULAR(self.titleLabel.font.pointSize);
}

@end
