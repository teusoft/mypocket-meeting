//
//  CountriesController.m
//  SOSPro
//
//  Created by Toan M. Ha on 12/9/13.
//  Copyright (c) 2013 Automagi. All rights reserved.
//

#import "CountriesController.h"

@interface CountriesController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation CountriesController
@synthesize dataSource, curCountryCode, delegate;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"];
    dataSource = [NSArray arrayWithContentsOfFile:plistPath];
}




-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    for (int i = 0; i < self.dataSource.count; i ++) {
        if ([self.curCountryCode isEqualToString:
             [[self.dataSource objectAtIndex:i] objectForKey:@"country"]]) {
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]
                             atScrollPosition:UITableViewScrollPositionMiddle
                                     animated:YES];
            return;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (!self.dataSource) {
        return 0;
    }
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CountryCell"];
        cell.accessoryType = UITableViewCellAccessoryNone;

    }
    NSDictionary* dic = [dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = [dic objectForKey:@"country"];
    
//    if ([[dic objectForKey:@"country"] isEqualToString:self.curCountryCode]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor clearColor];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:222.0/255.0
                                                           green:222.0/255.0
                                                            blue:222.0/255.0
                                                           alpha:0.9];
    }
    return cell;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
    NSDictionary* dic = [dataSource objectAtIndex:indexPath.row];
    if (self.delegate) {
        if ([[dic objectForKey:@"country_code"] isEqualToString:@"NONE"]) {
            [self.delegate selectCountry:[dic objectForKey:@""]];
        }else{
            [self.delegate selectCountry:[dic objectForKey:@"country"]];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}



@end
