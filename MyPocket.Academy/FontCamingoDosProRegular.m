//
//  Font-CamingoDosPro-Regular.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/3/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "FontCamingoDosProRegular.h"

@implementation FontCamingoDosProRegular

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = MPM_FONT_PRO_REGULAR(self.font.pointSize);
}

@end
