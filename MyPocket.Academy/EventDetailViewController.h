//
//  EventDetailViewController.h
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/24/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPMCalendarObject.h"

@class EventDetailViewController;

@protocol EventDetaiDelegate

- (void)skipEvent;
- (void)eventDetailDidTapToRateButtonForEvent:(MPMCalendarObject*)currentEvent;

@end

@interface EventDetailViewController : UIViewController

@property (strong, nonatomic) MPMCalendarObject *currentEvent;

@property (strong, nonatomic) IBOutlet UIView *addCommentView;
@property (strong, nonatomic) IBOutlet UILabel *viewTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;


@property (strong, nonatomic) IBOutlet UITextView *txtTextComment;

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated;
- (void)showPopUpWithTitle:(NSString*)title atDate:(NSDate*)mDate inView:(UIView *)aView animated:(BOOL)animated;

@property(nonatomic, strong)id<EventDetaiDelegate>delegate;


@property (strong, nonatomic) IBOutlet UIButton *btnRate;
@property (strong, nonatomic) IBOutlet UIButton *btnSkip;
- (IBAction)btnRateNowClicked:(id)sender;

@end
