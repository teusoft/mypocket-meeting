//
//  ServiceLib.m
//  MyPocket.Academy
//
//  Created by Luthor Nguyen on 10/31/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "ServiceLib.h"
#import "Lib.h"

static ServiceLib* serviceLib;
@implementation ServiceLib
@synthesize testMode, betaMode;
+(ServiceLib*)shareInstance {
	if (!serviceLib) {
		serviceLib = [[ServiceLib alloc] init];
	}
	return serviceLib;
}

+(NSString*)removeLogsSoapResponse:(NSString*)strSoap {
    if (!strSoap || strSoap.length < 10) {
        return strSoap;
    }
    NSRange range = [strSoap rangeOfString:@"<s:Envelope"
                                   options:NSCaseInsensitiveSearch
                                     range:NSMakeRange(10, strSoap.length - 10)];
    if (range.location != NSNotFound) {
        return [strSoap substringFromIndex:range.location];
    }
    return strSoap;
}
+(NSString*)sendGetRequest:(NSString*)strURL {
    //    NSLog(@"strURL: %@",strURL);
    
    //    for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies])
    //    {
    //        NSLog(@"name: '%@'\n",   [cookie name]);
    //        NSLog(@"value: '%@'\n",  [cookie value]);
    //        NSLog(@"domain: '%@'\n", [cookie domain]);
    //        NSLog(@"path: '%@'\n",   [cookie path]);
    //    }
    
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    [theRequest setHTTPShouldHandleCookies:YES];
	[theRequest setHTTPMethod:@"GET"];
    NSError* error;
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&error];
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
    //    NSLog(@"string: %@",string);
	return string;
}

+(NSString*)sendGetRequestWithHeader:(NSString*)strURL{
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    [theRequest setHTTPShouldHandleCookies:YES];
    
	[theRequest setHTTPMethod:@"GET"];
    NSError* error;
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&error];
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
    //    NSLog(@"string: %@",string);
	return string;
    
}

+(NSString*)sendGetRequestWithHeader:(NSString*)strURL andUserKey:(NSString*)key{

    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    [theRequest setHTTPShouldHandleCookies:YES];
//    [theRequest addValue:@"nUazeD2ThoJVkeum1Pwso79eZDadcEta" forHTTPHeaderField:@"x-header-reqauth"];
    [theRequest addValue:key forHTTPHeaderField:@"x-header-accid"];
    
	[theRequest setHTTPMethod:@"GET"];
    
    NSError* error;
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&error];
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];    
	
    return string;
}

+(NSString*)sendGetRequestLatinEncoding:(NSString*)strURL {
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
	[theRequest setHTTPMethod:@"GET"];
    NSError* error;
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&error];
    NSString *string = [[NSString alloc]
                        initWithData:responeData encoding: NSISOLatin1StringEncoding];
	return string;
}

+(NSString*)sendGetRequest:(NSString*)strURL withContent:(NSString*)strContent {
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
	[theRequest setHTTPMethod:@"POST"];
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSData* requestData = [strContent dataUsingEncoding:NSUTF8StringEncoding];
    NSString* requestDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]];
    [theRequest setHTTPBody: requestData];
    [theRequest setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
    
    NSError* error;
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&error];
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
	return string;
}


+(NSString*)sendRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL {
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    [theRequest setHTTPShouldHandleCookies:YES];

	[theRequest setHTTPMethod:@"POST"];
//    [theRequest addValue:@"nUazeD2ThoJVkeum1Pwso79eZDadcEta" forHTTPHeaderField:@"x-header-reqauth"];
	
	NSString* paramStr = @"";
	
	if (params) {
		NSEnumerator *keys = [params keyEnumerator];
		
		for (int i = 0; i < [params count]; i++) {
			NSString *key = [keys nextObject];
			NSString *val = [params objectForKey:key];
			paramStr = [NSString stringWithFormat:@"%@&%@=%@",paramStr,key,val];
		}
	}
	
	if (paramStr.length > 0) {
		NSData* requestData = [paramStr dataUsingEncoding:NSUTF8StringEncoding];
		NSString* requestDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]];
		[theRequest setHTTPBody: requestData];
		[theRequest setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
	}
	
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
	
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
	return string;
}

+(NSString*)sendRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL method:(NSString*)method{
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
	
	[theRequest setHTTPMethod:method];
	
	NSString* paramStr = @"";
	
	if (params) {
		NSEnumerator *keys = [params keyEnumerator];
		
		for (int i = 0; i < [params count]; i++) {
			NSString *key = [keys nextObject];
			NSString *val = [params objectForKey:key];
			paramStr = [NSString stringWithFormat:@"%@&%@=%@",paramStr,key,val];
		}
	}
	
	if (paramStr.length > 0) {
		NSData* requestData = [paramStr dataUsingEncoding:NSUTF8StringEncoding];
		NSString* requestDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]];
		[theRequest setHTTPBody: requestData];
		[theRequest setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
        if ([method isEqualToString:PUT_METHOD]) {
            [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        }
	}
	
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
	
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
	return string;
}

+(NSString*)postMultiPartRequest:(NSMutableArray*)params
                           files:(NSMutableArray*)files
                          andUrl:(NSString*)strURL {
    NSMutableURLRequest* theRequest = [[NSMutableURLRequest alloc] initWithURL:
                                       [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
	[theRequest setHTTPMethod:@"POST"];
	NSString *stringBoundary = @"0xKhTmLbOuNdArY";
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
	[theRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
	//create the body
	NSMutableData *postBody = [NSMutableData data];
	[postBody appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
    for (NSDictionary* dic in params) {
        [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[dic objectForKey:@"name"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[[NSString stringWithFormat:@"%@",[dic objectForKey:@"value"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    for (NSDictionary *dic in files) {
        NSData* data = [dic objectForKey:@"data"];
        if (data != nil) {
            [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [dic objectForKey:@"name"], [dic objectForKey:@"file_name"]]
                                  dataUsingEncoding:NSUTF8StringEncoding]];
            [postBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n", [dic objectForKey:@"content_type"]]
                                  dataUsingEncoding:NSUTF8StringEncoding]];
            [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postBody appendData:[NSData dataWithData:data]];
            [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
	[theRequest setHTTPBody:postBody];
    
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
	NSString *a = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
    return a;
}

+(NSString*)stringByRemoveDebug:(NSString *)str {
    NSRange range = [str rangeOfString:@"\n"];
    if (range.location != NSNotFound) {
        str = [str substringToIndex:range.location];
    }
    str = [str stringByTrimmingCharactersInSet:
           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return str;
}

+(NSString*)sendJsonRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL method:(NSString*)method{
    ///
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    
    [theRequest setHTTPMethod:method];
	
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [theRequest addValue:@"nUazeD2ThoJVkeum1Pwso79eZDadcEta" forHTTPHeaderField:@"x-header-reqauth"];
	
	NSString* paramStr = @"{";
	
	if (params) {
		NSEnumerator *keys = [params keyEnumerator];
		
		for (int i = 0; i < [params count]; i++) {
			NSString *key = [keys nextObject];
			NSString *val = [params objectForKey:key];
            if (i==(params.count-1)) {
                paramStr = [NSString stringWithFormat:@"%@\"%@\":\"%@\"",paramStr,key,val];
            }else{
                paramStr = [NSString stringWithFormat:@"%@\"%@\":\"%@\",",paramStr,key,val];
            }
		}
        
        paramStr = [NSString stringWithFormat:@"%@}",paramStr];
	}
	
	if (paramStr.length > 0) {
		NSData* requestData = [paramStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		NSString* requestDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]];
		[theRequest setHTTPBody: requestData];
		[theRequest setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
	}
	
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
	
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
	return string;
}

+(NSString*)sendJsonRequest:(NSMutableDictionary*)params andUrl:(NSString*)strURL andUserKey:(NSString*)key method:(NSString*)method{
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    
    [theRequest setHTTPMethod:method];
	
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [theRequest addValue:@"nUazeD2ThoJVkeum1Pwso79eZDadcEta" forHTTPHeaderField:@"x-header-reqauth"];
	[theRequest addValue:key forHTTPHeaderField:@"x-header-accid"];
    
	NSString* paramStr = @"{";
	
	if (params) {
		NSEnumerator *keys = [params keyEnumerator];
		
		for (int i = 0; i < [params count]; i++) {
			NSString *key = [keys nextObject];
			NSString *val = [params objectForKey:key];
            if (i==(params.count-1)) {
                paramStr = [NSString stringWithFormat:@"%@\"%@\":\"%@\"",paramStr,key,val];
            }else{
                paramStr = [NSString stringWithFormat:@"%@\"%@\":\"%@\",",paramStr,key,val];
            }
		}
        
        paramStr = [NSString stringWithFormat:@"%@}",paramStr];
	}
	
	if (paramStr.length > 0) {
		NSData* requestData = [paramStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		NSString* requestDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]];
		[theRequest setHTTPBody: requestData];
		[theRequest setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
	}
	
	NSData* responeData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
	
	NSString *string = [[NSString alloc] initWithData:responeData encoding:NSUTF8StringEncoding];
	return string;

}

@end
