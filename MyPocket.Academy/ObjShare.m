//
//  ObjShare.m
//  ExpensesEntry
//
//  Created by Lex on 6/24/13.
//  Copyright (c) 2013 Lex. All rights reserved.
//

#import "ObjShare.h"
#import "Lib.h"

#import "ServiceLib.h"
#import "JSONKit.h"
#import "AppDelegate.h"

@implementation ObjShare

@synthesize operationQueue;
@synthesize managedObjectContext;
@synthesize dateFormat;
@synthesize isOnMeeting;

+(ObjShare *)sharedInstance {
    static dispatch_once_t pred;
    static ObjShare *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[ObjShare alloc] init];
    });
    return shared;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        self.managedObjectContext = delegate.managedObjectContext;
        
        dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        isOnMeeting = NO;
    }
    return self;
}

@end