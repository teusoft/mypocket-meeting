//
//  IntroViewController.m
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/25/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageCurrent = 1;
    
    pageControlBeingUsed = NO;
    
    scrollContent.contentSize = CGSizeMake(scrollContent.frame.size.width*3, scrollContent.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnClose:(id)sender {
    
    [UIView animateWithDuration:.25f animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = scrollContent.frame.size.width;
        int page = floor((scrollContent.contentOffset.x - pageWidth / 3) / pageWidth) + 1;
        pageControl.currentPage = page;
        pageCurrent = page;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (IBAction)changePage:(id)sender {
    // Update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = scrollContent.frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = scrollContent.frame.size;
    [scrollContent scrollRectToVisible:frame animated:YES];
    
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

@end
