//
//  MyResultView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/26/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "MyResultView.h"

#import "MoodTimeline.h"
#import "AFNetworking.h"
#import "LCoreData.h"

#import "LCoreData.h"

#import "ServiceLib.h"
#import "Lib.h"
#import "JSONKit.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface MyResultView ()

@end

@implementation MyResultView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    profileObject = [LCoreData getProfile];
//    
//    if (!profileObject.isFacebook) {
//        profileObject.avatar = [NSString stringWithFormat:@"%@.png",profileObject.email];
//    }
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    profileObject = [LCoreData getProfile];

    //set name
    _lblName.text = profileObject.name;
    //    if ([profileObject.occupation length] == 0) {
    //        _lblOccupation.text = @"Occupation";
    //    } else {
    _lblOccupation.text = profileObject.occupation;
    //    }
    _lblEmail.text = profileObject.email;
    
    //last login
    
    NSString *lastLogin= profileObject.lastLogin;

    _lblLastLogin.text = [NSString stringWithFormat:[Lib localizedWithKey:@"MPM_KEY_LAST_LOGIN"],lastLogin];
    
    //set avatar
    self.imgAvatar.layer.cornerRadius = self.imgAvatar.frame.size.width / 2;
    self.imgAvatar.clipsToBounds = YES;
    self.imgAvatar.layer.borderWidth = 2.0f;
    self.imgAvatar.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.imgAvatar setImageWithURL:[NSURL URLWithString:profileObject.avatar]
              placeholderImage:[UIImage imageNamed:@"icon_avatarNoImage"]  usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    // end set avatar
    
    UIImage *img = [UIImage imageNamed:@"icon_result"];
    UIImage *imgSelected = [UIImage imageNamed:@"icon_resultSelected"];
    
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imgSelected = [imgSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    self.tabBarItem.image = img;
    self.tabBarItem.selectedImage = imgSelected;
    
//    _tabBarItemResult = [[UITabBarItem alloc] initWithTitle:@"My Result" image:img selectedImage:imgSelected];
    
    /*
    arrResult = [LCoreData getAllMoodTimeline];
    
    NSMutableArray *dataBefore = [NSMutableArray array];
    NSMutableArray *dataAfter = [NSMutableArray array];

    for(int i = 0; i < 4; i ++)
    {
        NSNumber *one = [NSNumber numberWithInt:0];
        [dataBefore addObject:one];
        [dataAfter addObject:one];
    }
    
    NSArray *arrMood = @[@"happy", @"calm", @"sad", @"angry"];

    for (MoodTimeline *moodItem in arrResult) {
        
        NSNumber *before = dataBefore[[arrMood indexOfObject:moodItem.moodBefore]];
        
        before = [NSNumber numberWithInt:[before intValue]+1];
        
        [dataBefore replaceObjectAtIndex:[arrMood indexOfObject:moodItem.moodBefore] withObject:before];


        NSNumber *after = dataAfter[[arrMood indexOfObject:moodItem.moodAfter]];

        after = [NSNumber numberWithInt:[after intValue]+1];
        
        [dataAfter replaceObjectAtIndex:[arrMood indexOfObject:moodItem.moodAfter] withObject:after];

    }
    
    if (!arrResult.count) {
        return;
    }
    
    for(int i = 0; i < 4; i ++){
        NSNumber *before = dataBefore[i];
        before = [NSNumber numberWithInt:100*[before intValue]/arrResult.count];
        [dataBefore replaceObjectAtIndex:i withObject:before];
        
        NSNumber *after = dataAfter[i];
        after = [NSNumber numberWithInt:100*[after intValue]/arrResult.count];
        [dataAfter replaceObjectAtIndex:i withObject:after];
    }
    
    NSArray *arrColor = @[[UIColor colorWithHexString:@"849f34"], [UIColor colorWithHexString:@"e5ae35"], [UIColor colorWithHexString:@"ff6600"], [UIColor colorWithHexString:@"cc0000"]];
    
    [self.chartBefore renderInLayer:self.chartBefore dataArray:dataBefore colorArray:arrColor];
    
    [self.chartAfter renderInLayer:self.chartAfter dataArray:dataAfter colorArray:arrColor];
*/
    //
    
    [self serviceGetTimeLineStatisticData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)serviceGetTimeLineStatisticData
{
    [Lib showIndicatorViewOn2:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", SERVICE_URL, GET_TIMELINE_STATISTIC_SERVICE];
    
    NSDictionary *params = @{
                             @"token": profileObject.userToken,
                             @"type":[NSNumber numberWithInteger:[segControl selectedSegmentIndex]]
                             };
    
    NSString* serviceTitle = @"GET TIMELINE STATISTIC";
    [Lib logRequestWithName:serviceTitle url:strUrl params:params];
    
    NSOperation* op = [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary* results = (NSDictionary *)responseObject;;
            BOOL isSuccess = [[results objectForKey:MPM_SERVICE_RESPONSE_IS_SUCCESS] boolValue];
            if (isSuccess) {
                NSArray* feeds = (NSArray *)[results objectForKey:MPM_SERVICE_RESPONSE_DATA];
                if (feeds) {
                    NSMutableArray *dataBefore = [NSMutableArray array];
                    NSMutableArray *dataAfter = [NSMutableArray array];
                    
                    for(int i = 0; i < 4; i ++)
                    {
                        NSNumber *one = [NSNumber numberWithInt:0];
                        [dataBefore addObject:one];
                        [dataAfter addObject:one];
                    }
                    NSArray *arrBefor, *arrAfter;
                    for (NSDictionary *dataItem in feeds) {
                        if ([[dataItem objectForKey:@"Key"] isEqualToString:@"Before"]) {
                            arrBefor = (NSArray*)[dataItem objectForKey:@"Value"];
                        }
                        if ([[dataItem objectForKey:@"Key"] isEqualToString:@"After"]) {
                            arrAfter = (NSArray*)[dataItem objectForKey:@"Value"];
                        }
                    }
                    for (NSDictionary *dataItem in arrBefor) {
                        [dataBefore replaceObjectAtIndex:[[dataItem objectForKey:@"Item"] intValue]-1 withObject:dataItem[@"Count"]];
                    }
                    for (NSDictionary *dataItem in arrAfter) {
                        [dataAfter replaceObjectAtIndex:[[dataItem objectForKey:@"Item"] intValue]-1 withObject:dataItem[@"Count"]];
                    }
                    if (arrBefor.count+arrAfter.count<1) {
                        lbNoData.hidden = NO;
                    }else{
                        lbNoData.hidden = YES;
                    }
                    NSArray *arrColor = @[[UIColor colorWithHexString:@"cc0000"], [UIColor colorWithHexString:@"ff6600"], [UIColor colorWithHexString:@"e5ae35"], [UIColor colorWithHexString:@"849f34"]];
                    
                    [self.chartBefore renderInLayer:self.chartBefore dataArray:dataBefore colorArray:arrColor];
                    
                    [self.chartAfter renderInLayer:self.chartAfter dataArray:dataAfter colorArray:arrColor];
                    
                    [Lib logResponseWithName:serviceTitle response:@"SUCCESS"];
                    
                }else{
                    [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
                }
            }else{
                [Lib logResponseWithName:serviceTitle response:[results objectForKey:MPM_SERVICE_RESPONSE_MESSAGE]];
            }
        }else{
            [Lib logResponseWithName:serviceTitle response:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
            [Lib showAlertMessage:MPM_ERROR_SERVER_NOT_RETURN_DATA_OR_RETURN_WITH_INCORRECT_FORMAT];
        }
        [Lib removeIndcatorViewOn:self.view];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Lib removeIndcatorViewOn:self.view];
        [Lib logResponseWithName:serviceTitle response: [error localizedDescription]];
        [Lib showAlertMessage: [error localizedDescription]];
    }];
    if (op == nil) {
        [Lib removeIndcatorViewOn:self.view];
    }
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    UIImage *img = [UIImage imageNamed:@"icon_result"];
    UIImage *imgSelected = [UIImage imageNamed:@"icon_resultSelected"];
    
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imgSelected = [imgSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item = [[UITabBarItem alloc] initWithTitle:[Lib localizedWithKey:@"MPM_KEY_MY_RESULT_TITLE"] image:img selectedImage:imgSelected];
}

- (IBAction)btnEditProfile:(id)sender {
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"editProfileView"];
    
    [self.navigationController pushViewController:controller animated:YES];    
}

- (UIImage*)loadAvatar
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:profileObject.avatar];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

- (NSString *)convertTimeStamp:(NSString *)timeStamp {
    
    double timestampval = [timeStamp doubleValue]/1000;
    NSTimeInterval timestamp = (NSTimeInterval)timestampval;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@"dd.MM.yyyy @ hh:mm"];
    NSString *_date=[_formatter stringFromDate:date];
    
    return _date;
}

- (IBAction)optionChanged:(id)sender {
    [self serviceGetTimeLineStatisticData];
}

@end
