//
//  SmileyView.m
//  MyPocket Meeting
//
//  Created by Nguyen Sy Tan on 12/11/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "SmileyView.h"

@implementation SmileyView



+ (id)initWithPoint:(CGPoint)point
{
    SmileyView *customView = [[[NSBundle mainBundle] loadNibNamed:@"SmileyView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[SmileyView class]]){
        
        customView.shouldBeRemoved = YES;
        
        [customView initSmileyLayout];
        customView.frame = CGRectMake(point.x,point.y, 320, 120);
        customView.isRemoved = NO;
        customView.startDate = [NSDate date];
        return customView;
    }else
        return nil;
}



#pragma -mark tanns SMILEY controls
- (void)initSmileyLayout
{
    
    mScrollView.contentSize = CGSizeMake(viewFinish.frame.size.width*2, mScrollView.frame.size.height);
    
    smileyFrame = [[NSMutableArray alloc] init];
    for (UIView* mV in smileyCollection) {
        [smileyFrame addObject:[NSNumber numberWithFloat:mV.frame.origin.x]];
        [self addSubview:mV];
    }
    
    viewMoodAfter.alpha = 0.0;
    
    isExpanded = NO;
    
    [sliderMoodAfter setThumbImage:[UIImage imageNamed:@"icon_slider"] forState:UIControlStateNormal];
    
    [sliderMoodAfter setMinimumTrackImage:[UIImage imageNamed:@"img_empty"] forState:UIControlStateNormal];
    [sliderMoodAfter setMaximumTrackImage:[UIImage imageNamed:@"img_empty"] forState:UIControlStateNormal];
    
    
    [self initViewForSmileys:0];
}

- (void)initViewForSmileys:(int)selectedSmiley
{
    
    btnCancelNewEvent.hidden = !_shouldBeRemoved && !isExpanded;
    // Not expand = cancel
    if (!isExpanded) {
        for (int i = 0; i < smileyCollection.count; i++) {
            UIView* mView = [smileyCollection objectAtIndex:i];
            CGRect mRect = mView.frame;
            mRect.origin.x = [[smileyFrame objectAtIndex:i] floatValue];
            
            
            [UIView animateWithDuration:0.3 animations:^{
                mView.alpha = 1.0;
                mView.frame = mRect;
            }];
        }
        
        
        if (mTimer) {
            [mTimer invalidate];
        }
        mTimer = nil;
        
        [mScrollView scrollRectToVisible:viewSetTime.frame animated:YES];
        mScrollView.userInteractionEnabled = NO;
        
        CGRect beforeFrame = self.frame;
        beforeFrame.size.height = 130;
        btnTipsNewEvent.enabled =NO;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            lblMoodSelectionHeader.alpha = 1.0;
            mScrollView.alpha = 0.0;
            lblAfter.alpha = 0.0;
            lblBefore.alpha = 0.0;
            self.frame =beforeFrame;
            btnTipsNewEvent.alpha = 0.0;
        }];
        
        viewMoodAfter.alpha = 0.0;
        viewMoodAfter.hidden= YES;
        
        txtMeetingName.text = @"";
        txtMeetingName.enabled = YES;
        
        [self.delegate smileyViewTableViewCellDidCancel:self];
        
    }else{ // Expand
        for (UIView* mView in smileyCollection) {
            CGRect mRect = mView.frame;
            mRect.origin.x = [[smileyFrame objectAtIndex:0] floatValue];
            [UIView animateWithDuration:0.3 animations:^{
                if (mView.tag == selectedSmiley) {
                    mView.alpha = 1.0;
                }else{
                    mView.alpha = 0.0;
                }
                mView.frame = mRect;
            }];
        }
        
        
        [mScrollView scrollRectToVisible:viewSetTime.frame animated:YES];
        mScrollView.userInteractionEnabled = YES;
        
        CGRect beforeFrame = self.frame;
        beforeFrame.size.height = 230;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            lblBefore.alpha = 1.0;
            lblMoodSelectionHeader.alpha = 0.0;
            mScrollView.alpha = 1.0;
            self.frame =beforeFrame;
        }];
        mAfterMood = -10;
        mBeforeMood = selectedSmiley;
        [self startTimer];
        [self.delegate smileyViewTableViewCell:self didStartWithMoodIndex:mBeforeMood];
        
    }
    isExpanded = !isExpanded;
}


- (IBAction)btnSmileyClicked:(id)sender {
    if (isExpanded) {
        if (txtMeetingName.text.length<1) {
            [Lib showAlert:nil withMessage:[Lib localizedWithKey:@"MPM_KEY_NEED_ENTER_THE_MEETING_NAME"]];
            [txtMeetingName becomeFirstResponder];
            return;
        }
        [txtMeetingName endEditing:YES];
        txtMeetingName.enabled = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            btnTipsNewEvent.alpha = 1.0;
        } completion:^(BOOL finished) {
            btnTipsNewEvent.enabled = YES;
            [self initViewForSmileys:(int)((UIView*)sender).tag];
        }];
        
        
    }
}


- (IBAction)btnCancelEventClicked:(id)sender
{
    if (!isExpanded || _shouldBeRemoved) {
        [self endEditing:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:MPM_ALERT_TITLE message:[Lib localizedWithKey:@"MPM_ALERT_ASK_TO_CANCEL"] delegate:self cancelButtonTitle:[Lib localizedWithKey:@"MPM_KEY_CANCEL"] otherButtonTitles:[Lib localizedWithKey:@"MPM_KEY_OK"],nil];
        alertView.tag = 1;
        [alertView show];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)startTimer {
    
    //nstimer backgound
    UIBackgroundTaskIdentifier bgTask;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    secondsCountDown = 4500;
    
    mTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                              target:self
                                            selector:@selector(updateTime)
                                            userInfo:nil
                                             repeats:YES];
}
- (void)updateTime {
    
    NSInteger minutes = floor(secondsCountDown / 60);
    NSInteger seconds = round(secondsCountDown % 60);
    
    NSString *strTimer = [NSString stringWithFormat:@"%02ld:%02ld",(long)minutes, (long)seconds];
    
    lblTimer.text = strTimer;
    
    NSLog(@"updateTime: %@", strTimer);
    
    secondsCountDown--;
    
    if (secondsCountDown < 0)
    {
        [mTimer invalidate];
        mTimer = nil;
        
        [self showPopupMoodAfter];
    }
}
- (void)showPopupMoodAfter {
    
    [self btnGoToFinish:nil];
    
}



- (IBAction)btnFinish:(id)sender {
    
    
    [self endEditing:YES];
    [txtMeetingName resignFirstResponder];
    
    mMoodTimeLine = [LCoreData creatNewMoodTimeline];
    
    NSDateFormatter* mDateFormatter = [[NSDateFormatter alloc] init];
    [mDateFormatter setDateFormat:MPM_TIME_LINE_DATE_FORMAT];
    
    mMoodTimeLine.title = txtMeetingName.text;
    mMoodTimeLine.isPosted = [NSNumber numberWithBool:NO];
    mMoodTimeLine.moodBefore = [NSNumber numberWithInt:mBeforeMood];
    mMoodTimeLine.moodAfter = [NSNumber numberWithInt:mAfterMood];
    mMoodTimeLine.dateTime = [NSDate date];
    mMoodTimeLine.showDateTime = [mDateFormatter stringFromDate:[NSDate date]];
    [LCoreData saveContext];
    [self initViewForSmileys:0];
    if (mTimer) {
        [mTimer invalidate];
        secondsCountDown = 4500;
        lblTimer.text = @"";
    }
    mTimer = nil;
    [self.delegate smileyViewTableViewCell:self didFinishWithMoodTimeLine:mMoodTimeLine];
}

- (IBAction)btnGoToFinish:(id)sender{
    
    if (mTimer) {
        [mTimer invalidate];
    }
    mTimer = nil;
    [mScrollView scrollRectToVisible:viewFinish.frame animated:YES];
    
    viewMoodAfter.hidden = NO;
    btnTipsNewEvent.enabled = NO;
    [UIView animateWithDuration:0.3 animations:^{
        viewMoodAfter.alpha = 1.0;
        btnTipsNewEvent.alpha = 0.0;
        lblAfter.alpha = 1.0;
    }];
    [self sliderUpdated:nil];
    
}

- (IBAction)btnChangeTimer:(id)sender {
    [self endEditing:YES];
    [self.delegate smileyViewTableViewCellDidTapTimer:self];
}

- (IBAction)btnTipClick:(id)sender {
    [self endEditing:YES];
    if ([Lib checkInternetConnectionWithMessage]) {
        int moodType = mAfterMood <  0 ? mBeforeMood : mAfterMood;
        [self.delegate smileyViewTableViewCellDidTapTips:self withId:moodType];
    }
}

- (IBAction)sliderUpdated:(id)sender{
    
    if (sender == nil) {
        sliderMoodAfter.value = 50;
        viewMoodAfter.image = [UIImage imageNamed:@"icon_moodCalm"];
        mAfterMood = MPM_SMILEY_YELLOW;
        return;
    }
    float value = sliderMoodAfter.value;
    if (value < 25) {
        viewMoodAfter.image = [UIImage imageNamed:@"icon_moodAngry"];
        mAfterMood = MPM_SMILEY_RED;
    }else if (value < 50) {
        viewMoodAfter.image = [UIImage imageNamed:@"icon_moodSad"];
        mAfterMood = MPM_SMILEY_ORANGE;
    }else if (value < 75) {
        viewMoodAfter.image = [UIImage imageNamed:@"icon_moodCalm"];
        mAfterMood = MPM_SMILEY_YELLOW;
    }else{
        viewMoodAfter.image = [UIImage imageNamed:@"icon_moodHappy"];
        mAfterMood = MPM_SMILEY_GREEN;
    }
    
}

- (void)changeTimer:(int)secondsChange
{
    secondsCountDown = secondsChange;
    [self updateTime];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self initViewForSmileys:0];
        if (mTimer) {
            [mTimer invalidate];
            secondsCountDown = 4500;
            lblTimer.text = @"";
        }
        mTimer = nil;
        
        [self.delegate smileyViewTableViewCellDidCancel:self];
    }
}

- (void)setMeetingTitle:(NSString *)title startAt:(NSDate *)startAt
{
    txtMeetingName.text = title;
    _startDate = startAt;
    
}


- (BOOL)checkIsOutOfTime
{
    if (_startDate && _shouldBeRemoved) {
        return [[NSDate date] compareToDate:[[NSDate date] dateByAddingTimeInterval:24*60*60]] > 0;
    }
    return NO;
}

- (void)setShouldBeRemoved:(BOOL)shouldBeRemoved
{
    _shouldBeRemoved = shouldBeRemoved;
//    lblMoodSelectionHeader.textColor = shouldBeRemoved ? [UIColor redColor] : [UIColor blackColor];
    txtMeetingName.enabled = btnShowDetail.hidden =!shouldBeRemoved;
    
    btnCancelNewEvent.hidden = !_shouldBeRemoved && !isExpanded;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}


- (void)showCancelButton:(BOOL)neededShow
{
    btnCancelNewEvent.hidden = !neededShow;
}

- (NSDate *)getDate
{
    return _startDate;
}
- (NSString *)getTitle
{
    return txtMeetingName.text;
}


- (IBAction)titleClicked:(id)sender
{
    [self.delegate smileyViewTableViewCellDidTapShowDetail:self withTitle:txtMeetingName.text atDate:_startDate];
}

@end
