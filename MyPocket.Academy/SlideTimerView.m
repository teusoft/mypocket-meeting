//
//  SlideTimerView.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 8/28/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "SlideTimerView.h"
#import "Lib.h"
#import "Profile.h"
#import "LCoreData.h"

@interface SlideTimerView ()

@end

@implementation SlideTimerView {
    Profile *profileObject;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.7];
    self.sliderView.layer.cornerRadius = 4;
    //    self.popUpView.layer.shadowOpacity = 0.8;
    //    self.popUpView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *sliderLeftTrackImage = [[UIImage imageNamed: @"bg_slideTimer"] stretchableImageWithLeftCapWidth: 9 topCapHeight: 0];
    UIImage *sliderRightTrackImage = [[UIImage imageNamed: @"bg_slideTimer"] stretchableImageWithLeftCapWidth: 9 topCapHeight: 0];
    [_slideTimer setMinimumTrackImage: sliderLeftTrackImage forState: UIControlStateNormal];
    [_slideTimer setMaximumTrackImage: sliderRightTrackImage forState: UIControlStateNormal];
    
    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"icon_slider"] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    profileObject = [LCoreData getProfile];
    int m= [profileObject.meetingTimer intValue] /60;
    _slideTimer.value = m;
    
    int minutes = (int)self.slideTimer.value;
    self.lblTimer.text = [NSString stringWithFormat:[Lib localizedWithKey:@"MPM_KEY_MINUTE_PARAM"],minutes];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (void)closePopup {
    [self removeAnimate];
}

- (void)showPopUp:(UIView *)aView animated:(BOOL)animated withTag:(int)tag
{
    _mtag = tag;
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

- (IBAction)btnOk:(id)sender {
    
    [self removeAnimate];
    int seconds = ((int)self.slideTimer.value)*60;
    profileObject.meetingTimer = [NSNumber numberWithInt:seconds];
    
    [self.delegate changeTimer:seconds withTag:_mtag];
    
}

- (IBAction)btnCancel:(id)sender {
    [self removeAnimate];
    
//    [delegate popUpHelpViewDidSelect];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)slideValueChange:(id)sender {
    int minutes = (int)self.slideTimer.value;
    self.lblTimer.text = [NSString stringWithFormat:[Lib localizedWithKey:@"MPM_KEY_MINUTE_PARAM"],minutes];
}


@end
