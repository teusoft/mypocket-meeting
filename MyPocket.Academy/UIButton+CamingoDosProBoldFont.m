//
//  UIButton+CamingoDosProBoldFont.m
//  MyPocket Meeting
//
//  Created by Nguyen Sy Tan on 12/17/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "UIButton+CamingoDosProBoldFont.h"

@implementation UIButton_CamingoDosProBoldFont

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.titleLabel.font = MPM_FONT_PRO_BOLD (self.titleLabel.font.pointSize);
}

@end
