//
//  MoodTimeline.h
//  MyPocket Meeting
//
//  Created by Luthor Nguyen on 11/17/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MoodTimeline : NSManagedObject

@property (nonatomic, retain) NSDate * dateTime;
@property (nonatomic, retain) NSNumber * isPosted;
@property (nonatomic, retain) NSNumber * moodAfter;
@property (nonatomic, retain) NSNumber * moodBefore;
@property (nonatomic, retain) NSNumber * moodId;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * moodAfterColor;
@property (nonatomic, retain) NSString * moodBeforeColor;
@property (nonatomic, retain) NSString * showDateTime;

@end
