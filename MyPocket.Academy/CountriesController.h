//
//  CountriesController.h
//  SOSPro
//
//  Created by Toan M. Ha on 12/9/13.
//  Copyright (c) 2013 Automagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountriesControllerDelegate
-(void)selectCountry:(NSString*)countryCode;
@end

@interface CountriesController : UIViewController {
    IBOutlet UITableView* tableView;
}
@property (nonatomic, retain) NSArray* dataSource;
@property (nonatomic, retain) NSString* curCountryCode;
@property (nonatomic, assign) id<CountriesControllerDelegate> delegate;

@end
