//
//  Font-CamingoDosPro-LightItalic.m
//  MyPocket.Academy
//
//  Created by KhoaNN on 9/3/14.
//  Copyright (c) 2014 Khoa Nguyen. All rights reserved.
//

#import "FontCamingoDosProLightItalic.h"

@implementation FontCamingoDosProLightItalic

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = MPM_FONT_LIGHT_ITALIC(self.font.pointSize);
}

@end
